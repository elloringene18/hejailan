<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {

        $data = [
            [
                'name' => 'Home',
            ],
            [
                'name' => 'About Us',
            ],
            [
                'name' => 'Our Business Sectors',
            ],
            [
                'name' => 'Our Companies',
            ],
            [
                'name' => 'News & Updates',
            ],
            [
                'name' => 'Contact Us',
            ],
            [
                'name' => 'AR',
            ],
            [
                'name' => 'Others',
            ],
        ];

        $pageorder = 1;
        foreach ($data as $page){
            $pageData['name'] = $page['name'];
            $pageData['order'] = $pageorder;
            $pageData['slug'] = \Illuminate\Support\Str::slug($page['name']);

            $newpage = \App\Models\Page::create($pageData);

            $pageorder++;
        }
    }
}
