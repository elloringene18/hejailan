<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Seeder;
use Psy\Util\Str;

class ClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */

    public function run()
    {
        $data = [
            [
                'name' => 'Wood Al Hejailan',
                'photo' => 'img/companies/wood.png',
                'url' => '',
            ],
            [
                'name' => 'Armetal',
                'photo' => 'img/companies/armtel.png',
                'url' => 'http://www.armetalssp.com/',
            ],
            [
                'name' => 'Armetal 2',
                'photo' => 'img/companies/armtel-ar.png',
                'url' => 'http://www.armetal.com/',
            ],
            [
                'name' => 'Biolab',
                'photo' => 'img/companies/biolab.png',
                'url' => 'http://www.biolabarabia.com/',
            ],
//            [
//                'name' => 'Enjaz',
//                'photo' => 'img/companies/enjaz.png',
//                'url' => '',
//            ],
            [
                'name' => 'Horizon Teleports',
                'photo' => 'img/companies/horizon.png',
                'url' => 'https://www.horizon-teleports.com/',
            ],
            [
                'name' => 'Sails',
                'photo' => 'img/companies/sails.png',
                'url' => 'http://www.sails.net/',
            ],
            [
                'name' => 'Swedtel',
                'photo' => 'img/companies/swedtel.png',
                'url' => 'http://www.swedtel.com/',
            ],
            [
                'name' => 'Trindel',
                'photo' => 'img/companies/trindel.png',
                'url' => '',
            ],
        ];

        foreach ($data as $item){
           Client::create($item);
        }
    }
}
