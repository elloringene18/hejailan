<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TeamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'EMAD J. AL-HEJAILAN
',
                'title' => 'CHAIRMAN',
                'photo' => 'img/about/emad.jpg',
            ],
            [
                'name' => 'FAISAL J. AL-HEJAILAN
',
                'title' => 'CEO & PRESIDENT
',
                'photo' => 'img/about/faisal.jpg',
            ],
            [
                'name' => 'WALEED J. AL-HEJAILAN
',
                'title' => 'COO
',
                'photo' => 'img/about/waleed.jpg',
            ],
            [
                'name' => 'WALID J. MERIE
',
                'title' => 'CFO
',
                'photo' => 'img/about/walid.jpg',
            ],
            [
                'name' => 'TALA E. AL-HEJAILAN
',
                'title' => 'LEGAL COUNCIL
',
                'photo' => 'img/about/tala.jpg',
            ],
            [
                'name' => 'RAKAN N. ABUDAFF
',
                'title' => 'SENIOR VP
',
                'photo' => 'img/about/rakan.jpg',
            ],
            [
                'name' => 'ABDULRAHMAN H. AL-WABEL
',
                'title' => 'GENERAL MANAGER - BIOLAB ARABIA LTD.
',
                'photo' => 'img/about/abdulrah.jpg',
            ],
            [
                'name' => 'SA’EED AL-SHAHRANI
',
                'title' => 'ITALMATCH BIOLAB INDUSTRIAL CO. (IBIC) - CEO
',
                'photo' => 'img/about/saeed.jpg',
            ],
            [
                'name' => 'HENRIK PRAME
',
                'title' => 'SWEDTEL ARABIA LTD. (SWEDTEL) - PRESIDENT
',
                'photo' => 'img/about/henrik.jpg',
            ],
            [
                'name' => 'KHEIREDDINE ARFI
',
                'title' => 'TRINDEL ARABIA LTD. - GENERAL MANAGER
',
                'photo' => 'img/about/kheireddi.jpg',
            ],
            [
                'name' => 'ALBERT HAYECK
',
                'title' => 'ARMETAL - MANAGING DIRECTOR
',
                'photo' => 'img/about/albert.jpg',
            ],
            [
                'name' => 'MOSTAFA EL-FAROUK
',
                'title' => 'HORIZON',
                'photo' => 'img/about/mostafa.jpg',
            ],
            [
                'name' => 'AHMED W. MEREI
',
                'title' => 'S.A.I.L.S - COO
',
                'photo' => 'img/about/ahmed.jpg',
            ],
        ];

        $order = 1;
        foreach ($data as $index => $item){
            $item['order'] = $order;
            \App\Models\Team::create($item);
            $order++;
        }
    }
}
