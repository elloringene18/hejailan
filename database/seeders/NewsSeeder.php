<?php

namespace Database\Seeders;

use App\Models\News;
use App\Services\CanCreateSlug;
use Faker\Generator;
use Illuminate\Database\Seeder;
use Symfony\Component\Console\Output\ConsoleOutput;

class NewsSeeder extends Seeder
{
    use CanCreateSlug;

    public function __construct(News $model)
    {
        $this->model = $model;
    }
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $output = new ConsoleOutput();

        $data = [];

        $data = [
            [
                'title' => 'Philips Buys 51% Stake In Saudi Lighting Firm
',
                'content' => '<p class="small">
</p><p>Netherlands-based Royal Philips announced Monday that it has entered into agreements to acquire 51 percent of General Lighting Company (GLC), a lighting company in Saudi Arabia for $235 million (on a cash-free, debt-free basis) plus additional transaction costs.</p>
<p>Philips will buy the GLC stake from a consortium of shareholders including Alliance Holding, the Hejailan Group, and The Carlyle Group, it said in a statement.</p>
<p>Netherlands-based Royal Philips announced Monday that it has entered into agreements to acquire 51 percent of General Lighting Company (GLC), a lighting company in Saudi Arabia for $235 million (on a cash-free, debt-free basis) plus additional transaction costs.</p>
<p><img alt="" src="https://wp.appliconsoft.com/hejailan/wp-content/uploads/2020/10/hejailan_philips-1.jpg" style="border-style:solid; border-width:1px; float:left; height:137px; margin:10px; width:200px">Philips will buy the GLC stake from a consortium of shareholders including Alliance Holding, the Hejailan Group, and The Carlyle Group, it said in a statement.</p>
<p>Under the terms of the agreements, Philips’ current lighting activities in the Kingdom will be combined with GLC forming a joint venture named Philips Lighting Saudi Arabia. Alliance Holding will be the JV partner with a 49 percent stake.</p>
<p>The Carlyle Group said it has entered into a definitive agreement to sell its 30 percent stake in General Lighting Company (GLC) to Royal Philips. In addition, shareholders Alliance Holding Ltd (Alliance) and the Hejailan Group have also agreed to sell holdings alongside The Carlyle Group, resulting in a majority sale.</p>
<p>During Carlyle’s four year tenure at GLC, several successful initiatives were launched that helped transform GLC’s Saudi-focused business into an international lighting player active in more than thirty markets, including the acquisition of the largest lighting company in Malaysia, Davex and establishing an experienced African sales team and distributor network in key cities which allowed GLC to sell products in 12 African countries. Production was doubled to 15 million units/year through the building of a third factory in Riyadh, dedicated to manufacturing high volume lighting products.</p>
<p>Firas Nasir, Managing Director and Co-head of the Carlyle MENA team, said: “Through close collaboration with our partners and GLC’s talented management team Carlyle fostered a number of successful initiatives at the company, expanding geographic presence and enhancing operational capabilities.</p>
<p><img alt="" src="https://wp.appliconsoft.com/hejailan/wp-content/uploads/2020/10/hejailan_glc-1.jpg" style="border-style:solid; border-width:1px; float:left; height:133px; line-height:20.7999992370605px; margin:10px; opacity:0.9; width:200px"></p>
<p>GLC is one of several investments Carlyle has made in the Middle East and Turkey and we continue to see excellent opportunities in the region.”</p>
<p>The new JV will employ approximately 1,300 people, the statement said.</p>
<p>It will focus on LED products, systems and services for the lighting market and will manufacture and distribute professional LED lighting fixtures across the value chain.</p>
<p>It will also create growth opportunities in sustainable technologies to support the government’s objective to reduce energy consumption, the statement added.</p>
<p>The deal is subject to customary regulatory approvals and is expected to be completed later this year. Moelis &amp; Company and Norton Rose Fulbright advised Philips, Gibson Dunn advised The Carlyle Group and the Hejailan Group and Baker &amp; McKenzie advised Alliance Holding on the deal. GIB Capital advised all sellers.</p>
<p>Saudi’s lighting market is forecast to grow strongly between 2014- 2018, thanks to targeted spending on construction and government and private investments in energy efficient lighting initiatives, said Philips.</p>
<p>“By partnering with GLC, Philips will be able to grow its business in this important market, particularly in relation to LED lighting,” said Eric Rondolat, CEO of Philips Lighting.</p>
<p>“We expect LED penetration in the KSA to achieve strong double-digit growth by 2018, driven by investments in public and private infrastructure.”</p>
<p>Philips, which has been present in KSA since 1935, is looking to expand its footprint. In 2012, the company announced a joint venture with Al Faisaliah Medical Systems to sell Philips Healthcare solutions and services in the country.</p>',
                'photo' => 'img/news/6.jpg',
                'thumbnail' => 'img/news/66.jpg',
            ],
            [
                'title' => 'Official Inauguration Of Horizon Teleports In Moosburg, Germany
',
                'content' => ' <p><strong style="line-height:1.6em">Moosburg, Munich, Germany, 15 May 2014</strong><span style="line-height:1.6em"> – HorizonSat has officially opened its new teleport – Horizon Teleports located north of Munich – implementing the latest state-of-the-art satellite technology and network infrastructure.</span></p>
<p>Horizon Teleports, a division of Dubai-based HorizonSat, is the result of the vertical integration of ground services into HorizonSat’s current broad, satellite services operations. HorizonSat is a well-known and experienced satellite communications provider and serves corporate, governmental and military clients as well as Telco Operators, ISPs and TV Broadcasters throughout the world, especially in the EMEA region. The state-of-the-art- teleport allows HorizonSat to enlarge the scope of its service offerings to existing and potential customers in the field of transmission and digital media services.</p>
<p><img alt="" src="https://wp.appliconsoft.com/hejailan/wp-content/uploads/2020/10/ht_opening_ceremony0.jpg" style="border-style:solid; border-width:1px; float:left; height:167px; margin:10px; width:250px">The new teleport, located in Moosburg, north of Munich, Germany, was officially inaugurated on 15th May 2014 with guests like clients, partners and suppliers from all over the world. The event started with delicious welcome drinks and photographs of the guests, followed by guided tours through the modern building, the network operation center, the high-tec server room and antenna park on the 17.000 sqm land.</p>
<p><img alt="" src="https://wp.appliconsoft.com/hejailan/wp-content/uploads/2020/10/ht_opening_ceremony2.jpg" style="border-style:solid; border-width:1px; float:right; height:167px; line-height:20.7999992370605px; margin:10px; opacity:0.9; width:250px"></p>
<p>The evening program commenced in an athmospheric and nicely decorated tent with speeches of Manuela Leitner – General Manager Horizon Teleports, Waleed Al Heijailan – CEO and Chairman of HorizonSat Group and Josef Dollinger – Deputy Mayor of Moosburg. The guests viewed an image film of the construction of Horizon Teleports. In the course of the evening a delicious dinner was served, followed by talks and drinks at the bar.</p>
<p><img alt="" src="https://wp.appliconsoft.com/hejailan/wp-content/uploads/2020/10/ht_opening_ceremony3.jpg" style="border-style:solid; border-width:1px; float:right; height:166px; margin:10px; width:250px">In 2013, HorizonSat not only completed the construction on an undeveloped site in the Munich, Germany area, but also began delivering a significant amount of customer services and traffic. The company installed and began operating its first eight 7-meter and 9-meter antennas to service both existing customers and to be available as new customer requirements arose. Of the first eight, five are fixed and three are agile for occasional-use traffic. The company is continuing to build antennas, and will have 16 antennas installed by the end of 2014. Its plan is to have over 30 operational antennas over the next few years.</p>
<p><img alt="" src="https://wp.appliconsoft.com/hejailan/wp-content/uploads/2020/10/ht_opening_ceremony1.jpg" style="border-style:solid; border-width:1px; float:left; height:167px; line-height:20.7999992370605px; margin:10px; opacity:0.9; width:250px"></p>
<p>Horizon Teleports core services range from Up- and downlink services, Broadband, IP Trunking, GSM Backhauling, Carrier Monitoring to Media contribution and distribution, across Europe, Asia, the Middle East and Africa. The teleport provides access to C-Band, Ku-Band and Ka-Band satellites located at 55 degrees West to 78 degrees East, using well-known satellite partners such as Eutelsat, Intelsat, APT, RSCC, Yahsat and Africasat.</p>',
                'photo' => 'img/news/5.jpg',
                'thumbnail' => 'img/news/55.jpg',
            ],
            [
                'title' => 'BioLab Embarks On Four-Phase Expansion
',
                'content' => '<p>BioLab Arabia Ltd “BLA” is a Saudi Chemical company based in Jubail Industrial City.</p>
<p>BLA produces Antiscalants chemicals used in Thermal and RO desalination plants, in cooperation with UK based, BWA Water Additives company.</p>
<p>BLA presently has a formulating capacity of 48,000 MT, and is currently expanding its plant with a first phase investment, which will cost US$25 million.</p>
<p><img alt="Polymerization Reactor" class="img-responsive" src="https://wp.appliconsoft.com/hejailan/wp-content/uploads/2020/10/biolab_reactor.jpg" style="float:left; margin:10px; width:250px"></p>
<p>The heart of the expansion is a fully automated state of the art plant, with a 30 MT polymerization reactor, that will enable BLA to produce over 20,000 MT annually of polymers and copolymers.</p>
<p>BLA will use in its production, Glacial Acrylic Acid, which has been allocated to it by SAMCO, a joint venture of DOW, Tasnee and Sahara, also based in Jubail Industrial City.</p>
<p>The expansion plan will be over phases – the first phase will be completed by the end of 2015. The first phase also includes a building of 7500 Sq. Mt, to store Raw materials, and accommodate the filling line and finished drummed products.</p>
<p>To meet anticipated future growth in demand, all necessary structures and infrastructures and utilities required to install other reactors to double the capacity, will be completed in this first phase.</p>
<p>In the event more reactors are needed, the existing structure, and utilities are designed to be extended in order to again double the production capacity.</p>
<p>BLA decision to expand its facility was to produce its own requirement of Poly Carboxylic Acid “PCA”, and also to be a key supplier of Polymers and Copolymers to other industries in the GCC and MENA region.</p>
<p>Another reason for BLA decision to expand is that, In the last several years, there has been a tremendous growth in demand of various chemicals across the Middle East and North Africa region, and this growth offers reputable companies with brand name products numerous opportunities to sell their products solutions, however a key reason for their inability to do so are, price competitiveness, and presence in the market.</p>
<p>For some of these companies, the cost and time it will take to design and build a manufacturing facility, and setup an organization to sell and distribute their products, can be either not feasible, or a risky investment etc. This is where BLA can offer the opportunity to manufacture and market the products of such companies. BLA can do so at costs lower than exporting the same from overseas locations and can also extend to these companies the full advantages and benefits as if they were a local manufacturer.</p>
<p>These cost reduction is achieved through savings on shipping and freight, custom duties, logistics, labor, utilities and inventory holding costs, in addition to the possibility of getting tariff protection in some cases.</p>
<p>BLA focus will be to serve industries related to the Paint, Construction Material, Detergents and Oil field chemicals.</p>
<p>BLA can work as toll manufacturers, Manufacturers under License, distributors, or joint venture partners.</p>
<p>BLA is a wholly owned company by Al-Hejailan Group, www.hejailan.com , whose companies are in Manufacturing, consulting Engineering and Telecommunications.</p>
<hr>
<p>&nbsp;</p>
<p><img alt="" class="img-responsive" src="https://wp.appliconsoft.com/hejailan/wp-content/uploads/2020/10/biolab_expansion1.jpg" style="width:100%"> BioLab expansion</p>
<p>&nbsp;</p>
<p><img alt="" src="https://wp.appliconsoft.com/hejailan/wp-content/uploads/2020/10/biolab_expansion2.jpg" style="width:100%"> BioLab expansion</p>
<p>&nbsp;</p>
<p><img alt="" src="https://wp.appliconsoft.com/hejailan/wp-content/uploads/2020/10/biolab_expansion3.jpg" style="width:100%"> BioLab expansion</p>',
                'photo' => 'img/news/3.jpg',
                'thumbnail' => 'img/news/33.jpg',
            ],
            [
                'title' => 'King Abdullah Petroleum Studies And Research Centre (KAPSARC)
',
                'content' => '<p>The King Abdullah Petroleum Studies and Research Center (KAPSARC) is an independent, non-profit research institution dedicated to researching energy economics, policy, technology, and the environment across all types of energy. KAPSARC is located in Riyadh. The project was designed by the famous Zaha Hadid with a state of the art design containing irregular shapes and spaces intended to reveal the identity of the project.</p>
<p><img style="border-style: solid; border-width: 1px; float: right; height: 239px; margin: 10px; width: 200px;" src="https://wp.appliconsoft.com/hejailan/wp-content/uploads/2020/10/armetal_KAPSARC3.jpg" alt="">Armetal Metal Industries had the privilege &nbsp; of executing a contract &nbsp;worth S.R 80,000,000 in both phases of this project: phase 1 is the residential area which was executed by the Korean main contractor SK Engineering, and phase 2 the commercial area performed by Drake &amp; Scull. The scope of works crafted by Armetal was mainly architectural metal works including iconic custom made decorative items, in addition to 40,000 sq. m of Aluminium Composite Cladding. The cladding works proved to be challenging due to the asymmetrical shapes. Furthermore, most of the panels are in 3D and curved with irrelevant shapes and sizes.</p>
<p>The project execution was done under the surveillance of Saudi Aramco, the consultant, who recommended Armetal as their vendors to execute the works taking into consideration &nbsp;Armetal’s high quality standards applied in the project, and the tight time frame.</p>
<table border="0" cellpadding="0" cellspacing="0">
<tbody>
<tr>
<td><img alt="" src="https://wp.appliconsoft.com/hejailan/wp-content/uploads/2020/10/armetal_KAPSARC1.jpg" style="border-style:solid; border-width:1px; line-height:20.7999992370605px; margin:10px 0px; opacity:0.9; width:100%"></td>
<td><img alt="" src="https://wp.appliconsoft.com/hejailan/wp-content/uploads/2020/10/armetal_KAPSARC2.jpg" style="border-style:solid; border-width:1px; margin-left:10px; margin-right:10px; width:100%"></td>
</tr>
<tr>
<td>&nbsp;</td>
<td>&nbsp;</td>
</tr>
</tbody>
</table>',
                'photo' => 'img/news/1.jpg',
                'thumbnail' => 'img/news/11.jpg',
            ],
            [
                'title' => 'Aramco Awards Engineering Contract
',
                'content' => '<p>By Kevin Baxter &nbsp;</p>
                                       
<p>Three companies selected for five-year Maintain Potential Programme scheme</p>
<ul>
<li>Offshore contract of the largest in the region.&nbsp;</li>
<li>First time Aramco has chosen more than one company for the MPP.</li>
<li>Three signatories gives Aramco much more flexibility</li>
</ul>
<p>Saudi Aramco has awarded three international engineering consultancies the contract for its Maintain Potential Programme (MPP), the engineering services contract that focuses on offshore operations.</p>
<p>The three companies are:</p>
<ul>
<li>KBR (US)</li>
<li>Mustang Engineering (US)</li>
<li>WorleyParsons (Australia)</li>
</ul>
<p>MEED reported in February that the firms were the frontrunners for the long-term deal and this has now been made official. All three companies are signatories of Aramco’s General Engineering Plus (GES-Plus) &nbsp;</p>
<p>The MPP has traditionally been one of the largest engineering services contracts to be awarded in the kingdom and the decision to award three companies confirms the new scope would be even more expansive and include more companies.</p>
<p>“The main factor for having three companies is that Aramco now has a flexible engineering force that can focus on several key issues at any one time,” says a source familiar with the MPP. “This means that the day-to-day maintenance can be done while planning for projects.”</p>
<p>Aramco is expected to make an award in April and then give the successful bidders 90 days to mobilize workers. The deal will run for an initial five years with the option of a two-year extension.</p>
<p>Historically the MPP has run on for even longer. WorleyParsons is the current holder of the MPP contract and has employed 350 dedicated staff on the scheme since 2003. It was awarded a two-year extension in 2013 that will expire in the summer.</p>
<p>Aramco has extensive offshore operations on its Gulf coast including Safaniya, the world’s largest offshore oil field, and several large non-associated gas fields. Many of the Gulf fields are mature and likely to require extensive rehabilitation over the next decade to maintain production. Hundreds of new wells need to be drilled every year to maintain operations in shallow-water fields.</p>
<p>Saudi Aramco was not available for comment when contacted by MEED.</p>',
                'photo' => 'img/news/4.jpg',
                'thumbnail' => 'img/news/44.jpg',
            ],
            [
                'title' => 'Armetal Metal Industries New Plant',
                'content' => '<p>In response to the increase of high quality projects, and the numerous prestigious upcoming projects, Armetal’ s Board of Directors decided to &nbsp;purchase a land of 16,000 sq. m in Riyadh Industrial City, southern Riyadh, to establish its new facilities consisting of 6,000 sq. m of factory with the latest machineries and equipment, and 1,000 sq. m of offices.</p>
<p>The new plant will increase the capacity of Armetal allowing higher revenue whilst acquiring the highest shares from the upcoming projects. Securing a contract for the Riyadh Metro Project is the main goal of Armetal at the moment due to the complexity and the quality standards set for the project. The capabilities, experience, and facilities of Armetal will provide &nbsp;the ideal combination for such a project. In addition, there are 11 stadium under the design phase with Saudi Aramco which are expected to be launched by mid of 2015.</p>',
                'photo' => 'img/news/2.jpg',
                'thumbnail' => 'img/news/22.jpg',
            ],
            [
                'title' => 'Al-Hejailan, Dow JV to build polyacrylic acid and emulsion polymers plant in Saudi Arabia',
                'content' => '<p>Italmatch Biolab Industrial will use Italmatch\'s raw materials and technology and Biolab\'s local manufacturing infrastructure to produce and sell additives and final solutions for Industrial Water &amp; Process Treatment in the Gulf Region with strong focus on Desalination and O&amp;G local markets. </p>
<p>&nbsp;</p>
<p><span class="xn-location">GENOA, Italy</span>, <span class="xn-chron">Dec. 22, 2020</span> /PRNewswire/ --&nbsp;<b>Italmatch Chemicals Group</b>, a leading global specialty chemical group focused on the production and marketing of performance additives for water and process treatment, oil &amp; gas, industrial lubricants and plastics, and <b>Biolab Arabia Ltd</b>, who is also a leading manufacturing and service provider in the region that is part of Al-Hejailan Group based in <span class="xn-location">Saudi Arabia</span>, have signed a joint venture agreement to incorporate in the <span class="xn-location">Kingdom of Saudi Arabia</span>, <b>Italmatch Biolab Industrial LCC </b>with Italmatch holding 60%. The two chemical companies join forces to create a new regional Saudi leading player focused on local manufacturing, to produce and sell ready-to-use specialty chemicals and solutions for Industrial Water &amp; Process Treatment with a focus on Desalination (thermal and membrane based) applications in the Gulf Region as well as on Oil &amp; Gas. </p>
<p>&nbsp;</p><p><b>Italmatch Chemicals Group</b>&nbsp;has been working in the <span class="xn-location">Kingdom of Saudi Arabia</span> for several years. Through its engagement in the Kingdom, in addition to the present Biolab JV and including the incorporation of Saudiphos with local partner SADIG, Italmatch Group is strengthening its presence in the local market by establishing a full integrated phosphorus chemistry in KSA and consolidating its leadership in Desalination solutions through local production.&nbsp;</p>
<p>&nbsp;</p><p><b>Italmatch Chemicals Group\'s CEO, <span class="xn-person">Sergio Iorio</span>, </b>said: "<i>We know Biolab Arabia since many years of close cooperation, and we highly regard Biolab as the best possible Partner in Downstream local production for the Water &amp; Oil markets: this joint venture represents the opportunity to have local production and marketing in the <span class="xn-location">Kingdom of Saudi Arabia</span>.</i> <i>Italmatch is already creating and investing in a wide project on P-based chemistries full value chain, in line with the Kingdom\'s \'Vision 2030\' program. Our project represents best-in-class chemical chain, non-existing in KSA so far. The aim is to develop and complete a full manufacturing chain starting from Maaden´s Phosphate Rock to final Downstream additives for industrial water &amp; process end market applications in Jubail area and this JV perfectly close the full value chain of supply.</i>"</p>
<p>&nbsp;</p><p><b>Biolab Arabia Ltd.</b>, which is part of Al-Hejailan Group currently has two manufacturing units in the same plant producing under license from global leading technology providers with a combined production capacity of over 80,000 metric tons annually. Considered one of the leading Saudi Arabian petrochemical innovators in the water treatment and emulsion polymers field, operating world class facilities.</p>
<p>&nbsp;</p><p><b><span class="xn-person">Faisal J. Al-Hejailan</span></b>&nbsp;said, "<i>We at Al-Hejailan Group though our company Biolab Arabia are thrilled to announce this new milestone in our relationship. Our group\'s overall strategy is geared towards the development of our downstream chemical manufacturing capabilities in the Kingdom through partnering up with world leading technology providers and reinforce our commitment towards localizing the production, which falls in line with the Vision 2030 program.</i>"</p>
<img alt="" src="https://rt.prnewswire.com/rt.gif?NewsItemId=EN31962&amp;Transmission_Id=202012221211PR_NEWS_EURO_ND__EN31962&amp;DateId=20201222" style="border: 0.0px;width: 1.0px;height: 1.0px;"><p> SOURCE Italmatch Chemicals</p>',
                'photo' => 'img/news/7.jpg',
                'thumbnail' => 'img/news/77.jpg',
            ],
            [
                'title' => 'ITALMATCH CHEMICALS and BIOLAB ARABIA signed a joint venture agreement for the incorporation of ITALMATCH BIOLAB INDUSTRIAL, located in the Kingdom of Saudi Arabia',
                'content' => '<p>Italmatch Biolab Industrial will use Italmatch\'s raw materials and technology and Biolab\'s local manufacturing infrastructure to produce and sell additives and final solutions for Industrial Water &amp; Process Treatment in the Gulf Region with strong focus on Desalination and O&amp;G local markets. </p>
<p>&nbsp;</p><p><span class="xn-location">GENOA, Italy</span>, <span class="xn-chron">Dec. 22, 2020</span> /PRNewswire/ --&nbsp;<b>Italmatch Chemicals Group</b>, a leading global specialty chemical group focused on the production and marketing of performance additives for water and process treatment, oil &amp; gas, industrial lubricants and plastics, and <b>Biolab Arabia Ltd</b>, who is also a leading manufacturing and service provider in the region that is part of Al-Hejailan Group based in <span class="xn-location">Saudi Arabia</span>, have signed a joint venture agreement to incorporate in the <span class="xn-location">Kingdom of Saudi Arabia</span>, <b>Italmatch Biolab Industrial LCC </b>with Italmatch holding 60%. The two chemical companies join forces to create a new regional Saudi leading player focused on local manufacturing, to produce and sell ready-to-use specialty chemicals and solutions for Industrial Water &amp; Process Treatment with a focus on Desalination (thermal and membrane based) applications in the Gulf Region as well as on Oil &amp; Gas. </p>
<p>&nbsp;</p><p><b>Italmatch Chemicals Group</b>&nbsp;has been working in the <span class="xn-location">Kingdom of Saudi Arabia</span> for several years. Through its engagement in the Kingdom, in addition to the present Biolab JV and including the incorporation of Saudiphos with local partner SADIG, Italmatch Group is strengthening its presence in the local market by establishing a full integrated phosphorus chemistry in KSA and consolidating its leadership in Desalination solutions through local production.&nbsp;</p>
<p>&nbsp;</p><p><b>Italmatch Chemicals Group\'s CEO, <span class="xn-person">Sergio Iorio</span>, </b>said: "<i>We know Biolab Arabia since many years of close cooperation, and we highly regard Biolab as the best possible Partner in Downstream local production for the Water &amp; Oil markets: this joint venture represents the opportunity to have local production and marketing in the <span class="xn-location">Kingdom of Saudi Arabia</span>.</i> <i>Italmatch is already creating and investing in a wide project on P-based chemistries full value chain, in line with the Kingdom\'s \'Vision 2030\' program. Our project represents best-in-class chemical chain, non-existing in KSA so far. The aim is to develop and complete a full manufacturing chain starting from Maaden´s Phosphate Rock to final Downstream additives for industrial water &amp; process end market applications in Jubail area and this JV perfectly close the full value chain of supply.</i>"</p>
<p>&nbsp;</p><p><b>Biolab Arabia Ltd.</b>, which is part of Al-Hejailan Group currently has two manufacturing units in the same plant producing under license from global leading technology providers with a combined production capacity of over 80,000 metric tons annually. Considered one of the leading Saudi Arabian petrochemical innovators in the water treatment and emulsion polymers field, operating world class facilities.</p>
<p>&nbsp;</p><p><b><span class="xn-person">Faisal J. Al-Hejailan</span></b>&nbsp;said, "<i>We at Al-Hejailan Group though our company Biolab Arabia are thrilled to announce this new milestone in our relationship. Our group\'s overall strategy is geared towards the development of our downstream chemical manufacturing capabilities in the Kingdom through partnering up with world leading technology providers and reinforce our commitment towards localizing the production, which falls in line with the Vision 2030 program.</i>"</p>
<img alt="" src="https://rt.prnewswire.com/rt.gif?NewsItemId=EN31962&amp;Transmission_Id=202012221211PR_NEWS_EURO_ND__EN31962&amp;DateId=20201222" style="border: 0.0px;width: 1.0px;height: 1.0px;"><p> SOURCE Italmatch Chemicals</p>',
                'photo' => '',
                'thumbnail' => '',
            ],
            [
                'title' => 'Al-Hejailan and Dow form joint venture',
                'content' => '<p>Al-Hejailan Group and Dow have formed a joint venture to design, build and operate a polyacrylic acid (PAA) and emulsion polymers plant in the Kingdom of Saudi Arabia.</p>
                <p>&nbsp;</p><p>Al-Hejailan will hold a 75% stake in the joint venture, and Dow will have 25% ownership.</p><p>&nbsp;</p><p>This partnership will enable Al-Hejailan and Dow to meet growing customer demand for coatings and water treatment applications in the Kingdom of Saudi Arabia and the broader Middle East region, as well as penetrate new markets.</p> <p>&nbsp;</p><p>The plant is expected to have a capacity of 40 000 tpy and will be built and operated by Al-Hejailan Group in Plaschem Park. The facility will utilise Dow’s manufacturing technologies, and Dow will be responsible for the marketing of the plant’s production. Construction of the plant is expected to begin in 2021 and production is expected to come on-stream in 2023.</p> <p>&nbsp;</p><p>Faisal Al-Hejailan, chief executive officer of Al- Hejailan Group, said “At Al- Hejailan Group, we are excited about this investment, which fulfills an important milestone in our group’s overall strategy and drives towards the development of more robust and expansive downstream manufacturing capabilities in the Kingdom. It further strengthens our strategic partnership with Dow, as we work jointly to capture downstream opportunities that align with the Kingdom’s localisation programme and the overall Vision 2030 program.”</p> <p>&nbsp;</p><p>“This investment further reinforces Dow’s commitment to serving the region and promoting the downstream industry in the Kingdom of Saudi Arabia,” said Howard Ungerleider, president and chief financial officer for Dow. “This strategic partnership with Al-Hejailan builds on Dow’s long-standing presence in the Kingdom and supports the long-term growth potential we see for Dow and our partners throughout the region.”</p> ',
                'photo' => '',
                'thumbnail' => '',
            ],
        ];

        foreach ($data as $item)
        {
            $item['slug'] = $this->generateSlug($item['title']);
            News::create($item);
        }



    }
}
