<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ServiceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'name' => 'INDUSTRIES',
                'headline' => 'A NEW GENERATION OF INDUSTRIAL INNOVATION',
                'content' => 'From deep inside the <strong>Al-Hejailan</strong> facilities, the spirit of industrial innovation is enriching value chains, laboratories and industrial plants.
                                        Our businesses are an important part of national, regional and international supply chains: generating economic opportunity and supporting Saudi industries. From architectural metal products to antiscalant chemicals for water desalination, the <strong>Al-Hejailan Group companies are shaping the future of Saudi industrial innovation. </strong>',
                'items' => [
                    [
                        'title' => 'Armetal Metal Industries Company Ltd.',
                        'content' => 'Founded in 1985, Armetal Metal Industries Company Ltd. is the largest manufacturer and supplier of the most commonly used stainless steel products in the Middle East. Armetal is renowned throughout the region for its superior quality in architectural metal products.',
                        'image' => 'img/business/industries-armetal.png',
                    ],
                    [
                        'title' => 'ARMETAL STAINLESS PIPE CO.',
                        'content' => '
                                    Armetal Stainless Pipe “ASP” is the First and leading Saudi manufacturer of a wide range of stainless steel products in the Middle East. ASP manufactures welded process pipes, CRA line pipes decorative tubes, and fittings. The core competence of the company is the manufacturing of longitudinally welded pipes in Stainless Steel, Duplex-Grades, High Nickel, and Special Alloys such as Copper-Nickel.
                        ',
                        'image' => 'img/business/industries-pipe.png',
                    ],
                    [
                        'title' => 'BIOLAB ARABIA LTD.',
                        'content' => '
                                    BioLab Arabia (BLA) under license from BWA Water Additives U.K., produces and supplies antiscalant chemicals for both thermal and reverse osmosis desalination plants – highly innovative and effective solutions for today’s demanding water treatment needs.
                        ',
                        'image' => 'img/business/industries-biolab.png',
                    ],
                ]
            ],
            [
                'name' => 'ENGINEERING ',
                'headline' => 'AN ADVANCED ENGINEERING ECOSYSTEM',
                'content' => 'Al-Hejailan Group’s engineering capabilities are driven by more than 500 engineering specialists, designers, project managers, planners and material specialists working to create advanced engineering solutions across Saudi Arabia and the wider region.',
                'content_bottom' => '',
                'items' => [
                    [
                        'title' => 'WOOD AL HEJAILAN ENGINEERING',
                        'content' => '
                                    Wood Al Hejailan Engineering, which was formed after absorbing a majority interest in Al-Hejailan Consultants by Wood International, provides planning, engineering, and project management services to the Government of Saudi Arabia and multiple Industrial sectors in the Kingdom.
                        ',
                        'image' => 'img/business/Engineering-1.jpg',
                    ],
                ]
            ],
            [
                'name' => 'TELECOM',
                'headline' => 'PIONEERING GLOBAL COMMUNICATIONS ',
                'content' => 'The Al-Hejailan Group telecommunications companies are operating from wholly-owned state-of-the-art network operation centres, vast teleport facilities and satellite systems that enable everything from high-end antennae and subsystems to aeronautical connectivity and high-speed broadband.',
                'items' => [
                    [
                        'title' => 'HORIZONSAT',
                        'content' => '
                                    Established in 2001 and headquartered strategically in the United Arab Emirates, from where it operates its state-of-the-art Network Operations Center, HorizonSat provides satellite services as an effective means of transmitting and receiving data - including voice, files and IP-based applications and media content in the Middle East, Asia and Africa.
                        ',
                        'image' => 'img/business/Telecom-1.jpg',
                    ],
                    [
                        'title' => 'HORIZON TELEPORTS',
                        'content' => '
                                    Horizon Teleports is a world-class and secure teleport facility operated from Munich, Germany. Housing 16 high-end, technically sophisticated antennas ranging in sizes from 2.4 meters up to 9.4 meters and a top of the line 24/7 Network Operations Center, Horizon Teleports enables worldwide communication for business and government entities across the globe.
                        ',
                        'image' => 'img/business/Telecom-2.jpg',
                    ],
                    [
                        'title' => 'SWEDTEL INTERNATIONAL GROUP',
                        'content' => '
                                    Swedtel originates from the Scandinavian telecommunications leader Telia. Since 1968, more than 150 government and privately owned telecom companies have trusted Swedtel with over 500 long and short-term assignments and telecom projects around the world.
                        ',
                        'image' => 'img/business/Telecom-3.jpg',
                    ],
                ],
            ],
            [
                'name' => 'SERVICES ',
                'headline' => 'SERVICES: PERFORMANCE AND EFFICIENCY IN SERVICE EXECUTION',
                'content' => 'Energy and environmental solutions, concierge services, logistics, procurement solutions, automation technologies and complex structures – just some of the Al-Hejailan Group’s fast-growing portfolio of services companies geared towards supporting Saudi Arabian industry. ',
                'items' => [
                    [
                        'title' => 'TRINDEL',
                        'content' => '
                                    Trindel was established in 1991 in Riyadh, Saudi Arabia as part of Al-Hejailan Group’s Group of companies. As a GREEN BUILDING solutions provider, Trindel forms part of the Group’s portfolio of strategic ventures in the lighting and systems sector – complementing earlier investments that include Nardeen and General Lighting Company.
                        ',
                        'image' => 'img/business/Trindel_1.jpg',
                    ],
                    [
                        'title' => 'SAUDI ARABIAN INTEGRATED LOGISTIC SYSTEMS',
                        'content' => '
                                    Saudi Arabian Integrated Logistics Systems (SAILS) is a concierge services company established in Saudi Arabia to aid international and Saudi companies in both government and commercial sectors. Human resources, logistics and administrative affairs, business office support, facilities maintenance, site support, government relations, personnel support services, travel services, procurement, translation, vehicle leasing, accounting services, and janitorial support represent just some of SAILS’ diversified services.
                        ',
                        'image' => 'img/business/INTEGRATED-LOGISTIC-SYSTEMS.jpg',
                    ],
                ],
            ],
        ];

        foreach ($data as $index => $item){
            $service['name'] = $item['name'];
            $service['content'] = $item['content'];
            $service['headline'] = $item['headline'];

            $newService = \App\Models\Service::create($service);

            foreach ($item['items'] as $ser){
                $newService->items()->create($ser);
            }
        }
    }
}
