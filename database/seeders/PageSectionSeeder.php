<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PageSectionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // 'title','parent_sub_id','content'
        $data = [

                1 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'ADVANCING FUTURE SAUDI INDUSTRIES',
                        'type' => 'text'
                    ],
                    [
                        'title' => 'Intro Image',
                        'content' => 'img/home/slider.jpg',
                        'type' => 'image',
                    ],
                    [
                        'title' => 'Main Content',
                        'content' => '
                                <p>With a diversified portfolio of innovation-led companies, <strong>Al-Hejailan Group is a proud champion of ambitious Saudi manufacturing, engineering, technology and industry</strong>.</p>
                                <p>&nbsp;</p>
                                <p>Comprised of home-grown companies that employ over <strong>2,000 nationals and expatriates</strong>, Al-Hejailan enriches the national value chain. It does this through the innovation and adoption of high-grade technologies in engineering design and supervision, construction management, petrochemicals manufacturing, industry, logistics support and telecommunications.</p>
                                <p>&nbsp;</p>
                                <p>Through a passionate commitment to the values and objectives laid out in <strong>Saudi Vision 2030</strong>, Al-Hejailan Group is and will continue to serve in the national interest, as an enabler of socio-economic growth through the development and investment of Saudi industry and its forward-thinking, highly skilled workforce.</p>
                                <p>&nbsp;</p>
                                <p style="text-align: center"><img src="http://188.166.44.182/HEJ/0001/final/public/img/home/vision-icons.png" class=""></p>
                                <p>&nbsp;</p>
                        ',
                    ],
                ],
                2 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'ABOUT US',
                        'type' => 'text'
                    ],
                    [
                        'title' => 'Intro Image',
                        'content' => 'videos/about.mp4',
                        'type' => 'video',
                    ],
                    [
                        'title' => 'Main Content',
                        'content' => '
                        <h1 class="section-head">Al-Hejailan Group</h1>
                        <h2 class="sub-head">A Family Business Leading Saudi Innovation</h2>
                        <p>&nbsp;</p>
                        <p>
                        Since 1980, the <strong>Al-Hejailan Group</strong> has embraced the spirit of innovation in the Kingdom of Saudi Arabia. We invest in the future of Saudi industries by offering access and opportunities to the Kingdom\'s global partners.</p>
                        <p>&nbsp;</p>
                        <p>
                        While supporting the needs of a fast-changing society and national economy, our culture is driven by the fundamental values of fairness, trust, openness and cooperation. We are a family business that believes in accelerating national progress through the creation of skilled jobs and investment opportunities for talented Saudi nationals.</p>
                        <p>&nbsp;</p>
                        <p>
                        Building on the success of our first engineering firm, which provided planning and project management services to the government and the industrial sector 40 years ago, we have since partnered with leading multi-national companies to create a diversified portfolio of innovation-led businesses. Based in Riyadh, with regional offices throughout the GCC, <strong>Al-Hejailan Group</strong> is a proud champion of ambitious Saudi manufacturing, engineering, technological and industrial prowess.
                        </p>
                        ',
                    ],
                    [
                        'title' => 'Team Heading',
                        'content' => 'MEET OUR SENIOR TEAM',
                        'type' => 'text'
                    ],
                ],

                3 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'BUSINESS SECTORS',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Intro Image',
                        'content' => 'img/business/slider.jpg',
                        'type' => 'image',
                    ],
                    [
                        'title' => 'Main Content',
                        'content' => '<p>The <strong>Al-Hejailan Group</strong> consists of a robust portfolio of job-creating and growth-generating industrial companies across multiple Saudi and GCC locations. With headquarters in Riyadh and subsidiary management offices located throughout the Kingdom, the Group is constantly expanding and reinforcing its role as a market-maker in the region.</p>',
                    ],
                ],
                4 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'WELCOME TO THE FUTURE OF SAUDI INDUSTRY',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Intro Image',
                        'content' => 'img/companies/slider.jpg',
                        'type' => 'image',
                    ],
                    [
                        'title' => 'Main Content',
                        'content' => '<p>Since 1980, the <strong>Al-Hejailan Group</strong> has been a catalyst for innovation in Saudi-led industries – <strong>pioneering engineering design and supervision, construction management, industry, trading, logistics, support and telecommunications.</strong></p>',
                    ],
                ],
                5 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'NEWS',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Intro Image',
                        'content' => 'img/news/slider.jpg',
                        'type' => 'image',
                    ],
                ],
                6 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'CONTACT US',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Riyadh Address',
                        'content' => '<p><strong>RIYADH</strong></p>
                            <p style="">Al-Hejailan Group Headquarters</p>
                            <p style="">P.O Box 9176</p>
                            <p style="">9175, Salahuddine Al Ayoubi Street,</p>
                            <p style="">Al Hejailan Group Building</p>
                            <p style="">Saudi Arabia</p>
                            <p style="">Tel : +966-11-4761414</p>
                            <p style="">info@hejailan.com</p>',
                    ],
                    [
                        'title' => 'Dubai Address',
                        'content' => '<p><strong>DUBAI</strong></p>
                            <p style="">Al-Hejailan Group Regional Office</p>
                            <p style="">Burj Daman, 1st floor</p>
                            <p style="">Al Mustaqbal St,</p>
                            <p style="">Dubai International Financial Center</p>
                            <p style="">Za’abeel, Dubai, UAE</p>
                            <p style="">+971 4 513 4991</p>
                            <p style="">info@hejailan.com</p>',
                    ],
                ],
                7 => [
                    [
                        'title' => 'Intro Heading',
                        'content' => 'الحجيلان.. نحو مستقبل واعد للصناعة السعودية',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Intro Image',
                        'content' => 'img/interior.jpg',
                        'type' => 'image',
                    ],
                    [
                        'title' => 'Intro Content',
                        'content' => '
                        <p>تضم مجموعة الحجيلان تحت مظلتها شركاتٍ متنوعةً تضعها بكل فخرٍ في مصاف رواد قطاعات التصنيع والاتصالات والخدمات الهندسية والبتروكيماويات في المملكة العربية السعودية. وقد عُنِيَت المجموعة، منذ تأسيسها عام 1980، بالابتكار واعتبرته ضرورةً للنهوض بعددٍ من الصناعات التي تحتل المملكة مكانة بارزة على خارطتها عالمياً وأساساً تقوم عليه استثماراتها الرامية لتحقيق غدٍ أفضل لبلادنا المباركة.
                            توظف المجموعة من خلال شركاتها المحلية أكثر من ألفي كادرٍ من المواطنين والوافدين وتثري سلسلة القيمة الوطنية من خلال جهودها على صعيد تعزيز الابتكار وخلق فرص عمل نوعية وضخ استثمارات كبيرة في قطاعاتٍ إقليمية حيوية.
                        </p>',
                    ],
                    [
                        'title' => 'Image Content',
                        'content' => '<p>مجموعة الحجيلان شركةٌ عائليةٌ عريقة تؤمن أن السبيل الأمثل لتسريع وتيرة التقدم هو التطوير المستمر والقيادة الملهمة وتلبية احتياجات المجتمع ومواكبة تطوره والمساهمة في دعم اقتصاد وطني قائم على أسس العدالة وتكافؤ الفرص والثقة والشفافية.
                                تغطي أنشطة الشركات التابعة للمجموعة صناعاتٍ وقطاعات متنوعة تتضافر معاً لدعم الصناعة الوطنية في مجالات الخدمات الهندسية وإدارة المشاريع والاتصالات وتصنيع المنتجات المعدنية للأغراض الإنشائية والمعمارية ومواسير الصلب ومنتجات الطلاء والدهانات، فضلاً عن امتلاك المجموعة مصنعاً مخصصاً لإنتاج مضادات التكلس المستخدمة في تحلية المياه.
                            </p>',
                    ],
                    [
                        'title' => 'Body Image',
                        'content' => 'img/interior-img.png',
                        'type' => 'image',
                    ],
                    [
                        'title' => 'Bottom Content',
                        'content' => '<p>وانطلاقاً من التزامها الراسخ بقيم ومستهدفات رؤية السعودية 2030، تواصل مجموعة الحجيلان النهوض بدورها الوطني الرائد في تحفيز النمو الاجتماعي والاقتصادي من خلال التطوير المستمر والتفكير المستقبلي بقيادة نخبةٍ من الكوادر الوطنية والكفاءات العالمية.</p>',
                    ],
                ],
                8 => [
                    [
                        'title' => 'Copyright',
                        'content' => 'COPYRIGHTS © 2015 Al Hejailan. ALL RIGHTS RESERVED. PRIVACY POLICY',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Facebook Link',
                        'content' => '',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Twitter Link',
                        'content' => '',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'LinkedIn Link',
                        'content' => 'https://www.linkedin.com/company/al-hejailan-group/',
                        'type' => 'text',
                    ],
                    [
                        'title' => 'Instagram Link',
                        'content' => '',
                        'type' => 'text',
                    ],
                ],
        ];

        foreach($data as $key=>$sub){
            foreach($sub as $item){
                $item['page_id'] = $key;
                $item['is_visible'] = isset($item['is_visible']) ? $item['is_visible'] : 1;
                $item['type'] = isset($item['type']) ? $item['type'] : 'html';
                $item['slug'] = \Illuminate\Support\Str::slug($item['title']);

                \App\Models\PageSection::create($item);
            }
        }
    }
}
