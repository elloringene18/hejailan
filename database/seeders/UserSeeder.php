<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [ 'name' => 'Admin', 'email' => 'gene@thisishatch.com', 'password' => \Illuminate\Support\Facades\Hash::make('secret') ],
            [ 'name' => 'Admin', 'email' => 'admin@hejailan.com', 'password' => \Illuminate\Support\Facades\Hash::make('HEJ0001@2021!') ],
        ];

        foreach ($data as $index => $item){
            \App\Models\User::create($item);
        }
    }
}
