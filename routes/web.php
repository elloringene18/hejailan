<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', function () {
    return view('index');
});

Route::post('/form-submit','FormController@submit');

Route::get('admin/login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('admin/login', 'Auth\LoginController@login');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('news/{slug}', 'NewsController@show');

Route::group(['prefix' => 'admin','middleware' => 'auth'], function() {

    Route::group(['prefix' => 'sections','middleware' => 'auth'], function() {
        Route::get('/edit/{id}', 'Admin\SectionController@edit');
        Route::post('/update', 'Admin\SectionController@update');
        Route::get('/delete/{id}', 'Admin\SectionController@delete');
        Route::get('/delete-slide/{id}', 'Admin\SectionController@deleteSlide');
    });

    Route::group(['prefix' => 'home','middleware' => 'auth'], function() {
        Route::get('/', function(){ return redirect('admin/sections/edit/1'); });
        Route::post('/icons/update', 'Admin\HomeController@update');
    });

    Route::group(['prefix' => 'services','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\ServiceController@index');
        Route::get('/create', 'Admin\ServiceController@create');
        Route::post('/store', 'Admin\ServiceController@store');
        Route::post('/update', 'Admin\ServiceController@update');
        Route::get('/edit/{id}', 'Admin\ServiceController@edit');
        Route::get('/delete/{id}', 'Admin\ServiceController@delete');
        Route::get('/delete-item/{id}', 'Admin\ServiceController@deleteItem');
    });

    Route::group(['prefix' => 'news','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\NewsController@index');
        Route::get('/create', 'Admin\NewsController@create');
        Route::post('/store', 'Admin\NewsController@store');
        Route::post('/update', 'Admin\NewsController@update');
        Route::get('/edit/{id}', 'Admin\NewsController@edit');
        Route::get('/delete/{id}', 'Admin\NewsController@delete');
    });

    Route::group(['prefix' => 'clients','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\BenefitController@index');
        Route::get('/create', 'Admin\BenefitController@create');
        Route::post('/store', 'Admin\BenefitController@store');
        Route::post('/update', 'Admin\BenefitController@update');
        Route::get('/edit/{id}', 'Admin\BenefitController@edit');
        Route::get('/delete/{id}', 'Admin\BenefitController@delete');
    });

    Route::group(['prefix' => 'clients','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\ClientController@index');
        Route::get('/create', 'Admin\ClientController@create');
        Route::post('/store', 'Admin\ClientController@store');
        Route::post('/update', 'Admin\ClientController@update');
        Route::get('/edit/{id}', 'Admin\ClientController@edit');
        Route::get('/delete/{id}', 'Admin\ClientController@delete');
    });

    Route::group(['prefix' => 'our-team','middleware' => 'auth'], function() {
        Route::get('/', 'Admin\TeamController@index');
        Route::get('/create', 'Admin\TeamController@create');
        Route::post('/store', 'Admin\TeamController@store');
        Route::post('/update', 'Admin\TeamController@update');
        Route::get('/edit/{id}', 'Admin\TeamController@edit');
        Route::get('/delete/{id}', 'Admin\TeamController@delete');
    });

    Route::group(['prefix' => 'contacts','middleware' => 'auth'], function() {
        Route::get('/','Admin\ContactController@index');
        Route::get('/export','Admin\ContactController@export');
        Route::get('/view/{id}','Admin\ContactController@view');
        Route::get('/delete/{id}','Admin\ContactController@delete');
    });
});

Auth::routes();

Route::get('/ar', function () {
    $is_arabic = 1;
    return view('ar',compact('is_arabic'));
});

Route::get('/{page}', function ($page) {
    if (view()->exists($page)){
        $currentPage = $page;

        return view($page,compact('currentPage'));
    }

    return view('404');
});
