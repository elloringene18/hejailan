@extends('master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $data = $contentService->getPageSections(5); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">
    <style>
        #pageslider {
            background-image: url({{ $article->thumbnail ? asset('public/'.$article->thumbnail) : asset('public/img/news/slider.jpg')}});

        }
        .filter {
            padding: 0;
            margin: 15px 0;
            list-style: none;
            text-align: center;
        }

        .filter li {
            display: inline-block;
            margin: 0 15px;
        }

        .filter li a {
            font-size: 16px;
            color: #727272;
            text-transform: uppercase;
            font-weight: 400;
        }

        .filter li a.active {
            font-size: 16px;
            color: #c2c1c1;
            text-transform: uppercase;
            border-bottom: 1px solid #c2c1c1;
            padding-bottom: 5px;
        }
        .readmore {
            float: right;
            width: auto;
            margin-top: 20px;
        }

        .sub-head {
            color: #c2c1c1;
            font-weight: 400;
            text-transform: uppercase;
            font-size: 26px;
        }

        .main-head {
            color: #c2c1c1;
            font-weight: 900;
            text-transform: uppercase;
            font-size: 48px;
        }

        .intro {
            font-size: 16px;
            font-weight: 400;
            margin-top: 25px;
            margin-bottom: 25px !important;
            text-align: justify;
        }

        .date {
            font-size: 16px;
            font-weight: 300;
            color: #c2c1c1;
        }

        .article-head {
            font-size: 32px;
            margin-top: 30px;
        }

        .article {
            padding-bottom: 60px;
            position: relative;
            margin-bottom: 40px;
        }

        .article .readmore {
            bottom: 0;
            right: 0;
            position: absolute;
        }

        .filter-content {
            display: none;
        }

        .filter-content.active {
            display: inline-block;
            max-width: 1080px;
            text-align: left;
        }

        .art-content, .art-content p, .art-content span {
            color: #fff !important;
            font-family: Roboto !important;
            font-size: 16px !important;
        }


        section.content {
            margin-top: 100px !important;
        }
        
        #head, #menu-nav {
            top: 0;
        }
        
        .section-sub-head {
            text-transform: capitalize;
        }
        
        @media only screen and (max-width: 1080px) {

            .sub-head {
                font-size: 20px;
            }

            .main-head {
                font-size: 38px;
            }

            .article-head {
                font-size: 24px;
                margin-top: 30px;
            }

        }

        @media only screen and (max-width: 991px) {

            .main-head {
                font-size: 28px;
            }

            .intro {
                font-size: 14px;
            }

        }

        @media only screen and (max-width: 767px) {

            .article .readmore {
                position: relative;
            }

            .readmore.main {
                display: none;
            }

            .article {
                padding-bottom: 20px;
            }
        }
    </style>
@endsection


@section('content')
<!--
    <section id="pageslider">
        <div class="container relative">
            <div class="vcenter animate"  data-animation="slide-in-right-1" data-top="0">
                <h1>{!! $data['intro-heading'] !!}</h1>
            </div>
        </div>
        <span class="scrollicon heartbeat"></span>
    </section>
-->

    <section id="content" class="content mb-5 mt-5">
        <div class="container relative">
            <div class="row mt-4">
                <div class="col-md-12">
                    <br/>
                    <h2 class="section-sub-head ">{{ $article->title }}</h2>
                    <br/>
                    <div class="art-content">
                        {!! $article->content !!}
                    </div>
                    <br/>
                    <br/>
                </div>
                <a href="{{url('news')}}">< Back to news</a>
            </div>
        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('public') }}/js/inner.js"></script>
    <script>
        $('#pageslider').animate({opacity: 1}, 3000);
    </script>
@endsection




