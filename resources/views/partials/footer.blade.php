
@inject('contentService', 'App\Services\ContentProvider')
<?php $others = $contentService->getPageSections(8); ?>
        <footer id="foot">
            <div class="container">
                <div class="row">
                    <div class="col-md-7 animate mb-3"  data-animation="slide-in-left-1">
                        {{ $others['copyright'] }}
                        <br/>
                        <div class="socials">

                            @if($others['facebook-link'])
                                <a href="{{ $others['facebook-link'] }}" class="fa fa-facebook" target="_blank"></a>
                            @endif

                            @if($others['linkedin-link'])
                                <a href="{{ $others['linkedin-link'] }}" class="fa fa-linkedin" target="_blank"></a>
                            @endif

                            @if($others['twitter-link'])
                                <a href="{{ $others['twitter-link'] }}" class="fa fa-twitter" target="_blank"></a>
                            @endif

                            @if($others['instagram-link'])
                                <a href="{{ $others['instagram-link'] }}" class="fa fa-instagram" target="_blank"></a>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-5 animate"  data-animation="slide-in-right-1">
                        <div class="row">
                            <div class="col-md-6">
                                <ul>
                                    <li><a href="{{ url('about') }}">About Us</a></li>
                                    <li><a href="{{ url('business') }}">Our Businness Sectors</a></li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul>
                                    <li><a href="{{ url('companies') }}">Our Companies</a></li>
                                    <li><a href="{{ url('news') }}">News & Updates</a></li>
                                    <li><a href="{{ url('contact') }}">Contact Us</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        
        <script src="{{ asset('public') }}/js/vendor/jquery-1.11.2.min.js"></script>
        <script src="{{ asset('public') }}/js/vendor/bootstrap.min.js"></script>
        <script src="//cdn.jsdelivr.net/npm/simple-parallax-js@5.2.0/dist/simpleParallax.min.js"></script>
        <script src="{{ asset('public') }}/js/jQuery-inView.js"></script>
        <script src="{{ asset('public') }}/js/main.js"></script>
        @yield('js')
        <script>
            function checkVisability() {
                var row = $('.animate');
                row.each(function(){
                    if ($(this).inView("topOnly")) {
                        $(this).addClass($(this).attr('data-animation'));
                    } else {

                    }
                });

                var row = $('.animate-only');
                row.each(function(){
                    if ($(this).inView("topOnly")) {
                        $(this).addClass($(this).attr('data-animation'));
                    } else {

                    }
                });
            }
            checkVisability();

            $(window).scroll(function() {
                checkVisability();
            });

        var imgs = document.images,
            len = imgs.length,
            counter = 0;

        [].forEach.call( imgs, function( img ) {
            if(img.complete)
              incrementCounter();
            else
              img.addEventListener( 'load', incrementCounter, false );
        } );

        function incrementCounter() {
            counter++;
            if ( counter === len ) {
                $('.fog').animate({opacity: .8}, 5000);
                $('.sand').animate({opacity: 1}, 5000);
                $('#pageslider').animate({opacity: 1}, 3000);
            }
        }
    </script>
        
    <script>
        var image = document.getElementsByClassName('step-down');

        new simpleParallax(image, {
            overflow: true,
            orientation: 'down'
        });

        var image = document.getElementsByClassName('step-up');

        new simpleParallax(image, {
            overflow: true,
            orientation: 'up'
        });entation: 'down'

        var image = document.getElementsByClassName('step-left');

        new simpleParallax(image, {
            overflow: true,
            orientation: 'left'
        });

        var image = document.getElementsByClassName('step-right');

        new simpleParallax(image, {
            overflow: true,
            orientation: 'right'
        });

        var image = document.getElementsByClassName('step-right-full');

        new simpleParallax(image, {
            overflow: true,
            orientation: 'right',
            maxTransition: 99
        });

    </script>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-P5TF95X01G"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'G-P5TF95X01G');
    </script>
    </body>
</html>
