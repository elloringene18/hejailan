<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang=""> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>{{ !isset($metaTitle) ? 'Al-Hejailan' : $metaTitle }}</title>
        <meta name="description" content="{{ !isset($metaDesc) ? 'Al-Hejailan is a holding company with diversified business interests in various sectors in the Kingdom of Saudi Arabia and around the globe. The companies within the group operate as separate business entities, and are managed and directed by the holding company from its head quarters in Riyadh.' : $metaDesc }}">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="{{ asset('public') }}/apple-touch-icon.png"><!-- Add icon library -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <link rel="stylesheet" href="{{ asset('public') }}/css/bootstrap.min.css">
        <link rel="stylesheet" href="{{ asset('public') }}/fonts/stylesheet.css">
        <link rel="stylesheet" href="{{ asset('public') }}/css/animations.css">
        <link rel="stylesheet" href="{{ asset('public') }}/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="{{ asset('public') }}/css/main.4.css">
        <link rel="icon" href="{{ asset('public') }}/img/favicon.ico" sizes="32x32">

        <link rel="shortcut icon" href="img/favicon.png">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@300;400;700;900&display=swap" rel="stylesheet">

        <meta property="og:image" content="{{ asset('public/img/webimg.jpg') }}" />

        <style>
            .readmore {
                float: left;
                width: auto;
                font-family: Cairo;
                font-size: 17px;
            }
            .vcenter {
                top: 50%;
                position: absolute;
            }
            .burger {
                 display: inline-block;
                 cursor: pointer;
             }

            .bar1, .bar2, .bar3 {
                width: 28px;
                height: 4px;
                background-color: #fff;
                margin: 3px 0;
                transition: 0.4s;
                border-radius: 3px;
                float: right;
            }

            .bar2 {
                width: 22px;
            }

            .bar3 {
                width: 18px;
            }

            /* Rotate first bar */
            .change .bar1 {
                -webkit-transform: rotate(-45deg) translate(-9px, 6px) ;
                transform: rotate(-45deg) translate(-9px, 6px) ;
            }

            /* Fade out the second bar */
            .change .bar2 {
                opacity: 0;
            }

            /* Rotate last bar */
            .change .bar3 {
                -webkit-transform: rotate(45deg) translate(-8px, -8px) ;
                transform: rotate(45deg) translate(-8px, -8px) ;
            }
            body {
                font-family: 'Roboto';
                background-color: #120f0d;
                color: #fff;
                font-size: 17px;
            }
            #pageslider {
              background-image: url({{ asset('public') }}/img/home/slider.jpg);
            }
            
            .contact-wrap strong, .contact-wrap b {
                font-weight: 600;
            }
            
            #pageslider .dots {
                display: none;
            }
        </style>

        @yield('css')
        <script src="{{ asset('public') }}/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        <header id="head">
            <div class="container">
                <div class="row">
                    <div class="col-8">
                        @if(isset($is_arabic))
                            <a href="{{ url('/') }}" id="main-logo-text" class="show ar"></a>
                            <a href="{{ url('/') }}" id="main-logo-icon"></a>
                        @else
                            <a href="{{ url('/') }}" id="main-logo-icon"></a>
                            <a href="{{ url('/') }}" id="main-logo-text" class="show">AL-HEJAILAN</a>
                        @endif
                    </div>
                    <div class="col-3 langs">
                        <a href="{{ url('/') }}">EN</a> /
                        <a href="{{ url('/ar') }}">AR</a>
                    </div>
                    <div class="col-1">
                        <div class="burger" id="menu-icon">
                            <div class="bar1"></div>
                            <div class="bar2"></div>
                            <div class="bar3"></div>
                        </div>

                    </div>
                </div>
            </div>
        </header>
        
        <header id="menu-nav" class="">
            <div class="container">
                <div class="row">
                    <div class="col-md-9">
                        <ul class="vcenter animate" data-animation="slide-in-left-1" data-top="30">
                            <li><span class="count">01</span><a href="{{ url('about') }}">About Us</a></li>
                            <li><span class="count">02</span><a href="{{ url('business') }}">Our Business Sectors</a></li>
                            <li><span class="count">03</span><a href="{{ url('companies') }}">Our Companies</a></li>
                            <li><span class="count">04</span><a href="{{ url('news') }}">News & Updates</a></li>
                            <li><span class="count">05</span><a href="{{ url('contact') }}">Contact Us</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3">
                        <div class="contact-wrap vcenter animate"  data-animation="slide-in-right-1">
                            <p class="head">CONTACT</p>

                            @inject('contentService', 'App\Services\ContentProvider')
                            <?php $data = $contentService->getPageSections(6); ?>
                            {!! $data['riyadh-address'] !!}
                            <hr/>
                            {!! $data['dubai-address'] !!}
                            <div class="socials">
                                @inject('contentService', 'App\Services\ContentProvider')
                                <?php $others = $contentService->getPageSections(8); ?>

                                @if($others['facebook-link'])
                                    <a href="{{ $others['facebook-link'] }}" class="fa fa-facebook" target="_blank"></a>
                                @endif

                                @if($others['linkedin-link'])
                                    <a href="{{ $others['linkedin-link'] }}" class="fa fa-linkedin" target="_blank"></a>
                                @endif

                                @if($others['twitter-link'])
                                    <a href="{{ $others['twitter-link'] }}" class="fa fa-twitter" target="_blank"></a>
                                @endif

                                @if($others['instagram-link'])
                                    <a href="{{ $others['instagram-link'] }}" class="fa fa-instagram" target="_blank"></a>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
