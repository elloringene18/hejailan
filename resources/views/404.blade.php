@extends('master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $data = $contentService->getPageSections(1); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/home.css">
    <style>
        #pageslider {
            background-image: url({{asset('public/'.$data['intro-image'])}});
        }
    </style>
@endsection


@section('content')
    <section id="pageslider">
        <img src="{{ asset('public') }}/img/fog.png" class="fog">
        <img src="{{ asset('public') }}/img/sand.png" class="sand">
        <div class="container relative">
            <div class="vcenter animate"  data-animation="slide-in-right-1" data-top="0">
                <h1>PAGE NOT FOUND</h1>
            </div>
        </div>
        <span class="scrollicon heartbeat"></span>
        <div class="dots">
            <ul>
                <li><a href="#" class="active"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>
        </div>
    </section>
@endsection


@section('js')
@endsection




