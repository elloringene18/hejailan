@extends('master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $data = $contentService->getPageSections(5); ?>
<?php $news = $contentService->getNews(); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">
    <style>
        #pageslider {
            background-image: url({{ asset('public') }}/img/news/slider.jpg);

        }
        .filter {
            padding: 0;
            margin: 15px 0;
            list-style: none;
            text-align: center;
        }

        .filter li {
            display: inline-block;
            margin: 0 15px;
        }

        .filter li a {
            font-size: 20px;
            color: #727272;
            text-transform: uppercase;
            font-weight: 400;
        }

        .filter li a.active {
            font-size: 20px;
            color: #c2c1c1;
            text-transform: uppercase;
            border-bottom: 1px solid #c2c1c1;
            padding-bottom: 5px;
        }
        .readmore {
            float: right;
            width: auto;
            margin-top: 20px;
        }

        .sub-head {
            color: #c2c1c1;
            font-weight: 400;
            text-transform: uppercase;
            font-size: 18px;
        }

        .main-head {
            color: #c2c1c1;
            font-weight: 900;
            text-transform: uppercase;
            font-size: 36px;
        }

        .intro {
            font-size: 16px !important;
            font-weight: 400;
            margin-top: 14px;
            margin-bottom: 25px !important;
            color: #fff !important;
            font-family: Roboto !important;
        }

        .date {
            font-size: 20px;
            font-weight: 300;
            color: #c2c1c1;
        }

        .article-head {
            font-size: 22px;
            margin-top: 30px;
        }

        .article {
            padding-bottom: 60px;
            position: relative;
            margin-bottom: 40px;
        }

        .article .readmore {
            bottom: 0;
            left: 15px;
            position: absolute;
            font-size: 14px;
            padding: 8px 30px;
        }

        .readmore.main {
            font-size: 16px;
            padding: 8px 30px;
            float: left;
        }

        .filter-content {
            display: none;
        }

        .filter-content.active {
            display: inline-block;
            max-width: 1160px;
            text-align: left;
        }

        @media only screen and (max-width: 1080px) {

            .sub-head {
                font-size: 24px;
            }

            .main-head {
                font-size: 42px;
            }

            .article-head {
                font-size: 28px;
                margin-top: 30px;
            }

        }

        @media only screen and (max-width: 991px) {

            .main-head {
                font-size: 32px;
            }

            .intro {
                font-size: 18px;
            }

        }

        @media only screen and (max-width: 767px) {

            .article .readmore {
                position: relative;
            }

            .article {
                padding-bottom: 20px;
            }
        }
</style>
@endsection


@section('content')
    <section id="pageslider">
        <div class="container relative">
            <div class="vcenter animate"  data-animation="slide-in-right-1" data-top="0">
                <h1>{!! $data['intro-heading'] !!}</h1>
            </div>
        </div>
        <span class="scrollicon heartbeat"></span>
    </section>

    <section id="content" class=" mb-5">
        <div class="container relative text-center">
            <div class="row mt-4">
                <div class="col-md-12 text-center">
                    {{--<ul class="filter">--}}
                        {{--<li><a href="#" class="active">Latest News</a></li>--}}
                        {{--<li><a href="#">News Archive</a></li>--}}
                    {{--</ul>--}}
                </div>
            </div>
            <div class="filter-content active">
                <div class="row mt-4">
                    <div class="col-md-6">
                        <h4 class="sub-head">Latest News</h4>
                        <h2 class="main-head">{{ $news[0]->title }}</h2>
                        <!--                            <p class="date">March 16 2015</p>-->
                        <p class="intro">
                            {!! Str::words(strip_tags($news[0]->content),80)  !!}
                        </p>
                        <a href="{{ url('news/'.$news[0]->slug) }}#content" class="readmore main">Read more</a>
                    </div>
                    <div class="col-md-1"></div>
                    <div class="col-md-5">
                        <a href="{{ url('news/'.$news[0]->slug) }}#content">
                            <img class="mb-3" src="{{ $news[0]->photo ? asset('public/'.$news[0]->photo) : asset('public/img/news/placeholder.jpg')}}" width="100%;">
                        </a>
                    </div>
                </div>

                <div class="row mt-1">
                    <div class="col-md-12">
                    </div>
                </div>

                <div class="row mt-5 articles">
                    <?php $loops = 1; ?>
                    @foreach($news as $article)
                        @if($loops>1)
                            <div class="col-md-4 article">
                                <a href="{{ url('news/'.$news[0]->slug) }}#content">
                                    <img src="{{ $article->photo ? asset('public/'.$article->photo) : asset('public/img/news/placeholder.jpg')}}" width="100%;">
                                </a>
                                <h2 class="article-head">{{ Str::words($article->title,6) }}</h2>
                                <!--                            <p class="date">October 30 2015</p>-->
                                 <p class="intro">
                                     {!! Str::words(strip_tags($article->content),30)  !!}
                                 </p>
                                <a href="{{ url('news/'.$article->slug) }}#content" class="readmore">Read more</a>
                            </div>
                        @else
                            <?php $loops++; ?>
                        @endif
                    @endforeach
                </div>
            </div>
            <div class="filter-content">
                <div class="row mt-4">
                    <div class="col-md-5">
                        <h4 class="sub-head">Archive News</h4>
                        <h2 class="main-head">Armetal Metal Industries New Plant</h2>
                        <!--                            <p class="date">October 30 2015</p>-->
                        <p class="intro">In response to the increase of high quality projects, and the numerous prestigious upcoming projects, Armetal’ s Board of Directors decided to purchase a land of 16,000 sq. m in Riyadh Industrial City, southern Riyadh, to establish its new facilities consisting of 6,000 sq. m of factory with the latest machineries and equipment, and 1,000 sq. m of offices.
                        </p>
                        <a href="#" class="readmore main">Read more</a>
                    </div>
                    <div class="col-md-1">

                    </div>
                    <div class="col-md-6">
                        <img src="{{ asset('public') }}/img/news/news-main.png" width="100%;">
                    </div>
                </div>

                <div class="row mt-5">
                    <div class="col-md-12">
                    </div>
                </div>

                <div class="row mt-5 articles">
                    <div class="col-md-4 article">
                        <img src="{{ asset('public') }}/img/news/news-3.png" width="100%;">
                        <h2 class="article-head">BioLab Embarks On Four-Phase Expansion</h2>
                        <!--                            <p class="date">February 28 2015</p>-->
                        <p class="intro">BioLab Arabia Ltd “BLA” is a Saudi Chemical company based in Jubail Industrial City. BLA produces Antiscalants chemicals used in Thermal and RO desalination plants, in cooperation with UK based, BWA Water Additives company.
                            <br/>
                            BLA presently has a formulating capacity of 48,000 MT, and is currently expanding its plant with a first phase investment, which will cost US$25 million.
                        </p>
                        <a href="#" class="readmore">Read more</a>
                    </div>
                    <div class="col-md-4 article">
                        <img src="{{ asset('public') }}/img/news/news-2.png" width="100%;">
                        <h2 class="article-head">King Abdullah Petroleum Studies And Research Centre (KAPSARC)</h2>
                        <!--                            <p class="date">March 4 2015</p>-->
                        <p class="intro">The King Abdullah Petroleum Studies and Research Center (KAPSARC) is an independent, non-profit research institution dedicated to researching energy economics, policy, technology, and the environment across all types of energy. KAPSARC is located in Riyadh. The project was designed by the famous Zaha Hadid with a state of the art design containing irregular shapes and spaces intended to reveal the identity of the project.
                        </p>
                        <a href="#" class="readmore">Read more</a>
                    </div>
                    <div class="col-md-4 article">
                        <img src="{{ asset('public') }}/img/news/news-5.png" width="100%;">
                        <h2 class="article-head">Philips Buys 51% Stake In Saudi Lighting Firm</h2>
                        <!--                            <p class="date">March 18 2014</p>-->
                        <p class="intro">Netherlands-based Royal Philips announced Monday that it has entered into agreements to acquire 51 percent of General Lighting Company (GLC), a lighting company in Saudi Arabia for $235 million (on a cash-free, debt-free basis) plus additional transaction costs.
                            <br/>
                            Philips will buy the GLC stake from a consortium of shareholders including Alliance Holding, the Hejailan Group, and The Carlyle Group, it said in a statement.

                        </p>
                        <a href="#" class="readmore">Read more</a>
                    </div>
                    <div class="col-md-4 article">
                        <img src="{{ asset('public') }}/img/news/news-4.png" width="100%;">
                        <h2 class="article-head">Official Inauguration Of Horizon Teleports In Moosburg, Germany</h2>
                        <!--                            <p class="date">May 15 2014</p>-->
                        <p class="intro">HorizonSat has officially opened its new teleport – Horizon Teleports located north of Munich – implementing the latest state-of-the-art satellite technology and network infrastructure.
                            <br/>
                            Horizon Teleports, a division of Dubai-based HorizonSat, is the result of the vertical integration of ground services into HorizonSat’s current broad, satellite services operations. HorizonSat is a well-known and experienced satellite communications provider and serves corporate, governmental and military clients as well as Telco Operators, ISPs and TV Broadcasters throughout the world, especially in the EMEA region. 
                        </p>
                        <a href="#" class="readmore">Read more</a>
                    </div>
                    <div class="col-md-4 article">
                        <img src="{{ asset('public') }}/img/news/news-1.png" width="100%;">
                        <h2 class="article-head">Aramco Awards Engineering Contract</h2>
                        <!--                            <p class="date">March 16 2015</p>-->
                        <p class="intro">Saudi Aramco has awarded three international engineering consultancies the contract for its Maintain Potential Programme (MPP), the engineering services contract that focuses on offshore operations.
                            <br/>
                            The three companies are: KBR (US), Mustang Engineering (US) and WorleyParsons (Australia)
                        </p>
                        <a href="#" class="readmore">Read more</a>
                    </div>
                </div>
            </div>

        </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('public') }}/js/inner.js"></script>
    <script>
        $('#pageslider').animate({opacity: 1}, 3000);

        $('.filter a').on('click', function(e){
            e.preventDefault();

            $('.filter a.active').removeClass('active');
            $('.filter-content.active').removeClass('active');

            $('.filter-content').eq($(this).index('.filter a')).addClass('active');
            $(this).addClass('active');
        });

    </script>
@endsection


        

