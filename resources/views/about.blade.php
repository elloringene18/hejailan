@extends('master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $data = $contentService->getPageSections(2); ?>
<?php $team = $contentService->getTeam(); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">
    <style>
        #pageslider {
            background-image: none;
        }

        .sub-head {
            color: #c2c1c1;
            font-weight: 400;
            text-transform: uppercase;
            font-size: 26px;
        }

        .main-head {
            color: #c2c1c1;
            font-weight: 900;
            text-transform: uppercase;
            font-size: 58px;
        }

        #video {
            position: absolute;
            top: 0;
            width: auto;
            height: 100%;
            left: 50%;
        }

        #team img {
            width: 90%;
        }

        @media only screen and (max-width: 1080px) {

            .sub-head {
                font-size: 25px;
            }

            .main-head {
                font-size: 42px;
            }

        }

        @media only screen and (max-width: 991px) {

            .main-head {
                font-size: 32px;
            }
        }

        @media only screen and (max-width: 767px) {
            #team img {
                width: 100%;
            }
        }
    </style>
@endsection


@section('content')
    <section id="pageslider">
        <video autoplay muted loop playsinline id="video" class="">
            <source src="{{ asset('public/'.$data['intro-image']) }}" type="video/mp4">
        </video>
        <div class="container relative">
            <div class="vcenter animate"  data-animation="slide-in-right-1" data-top="0">
                <h1>{!! $data['intro-heading'] !!}</h1>
            </div>
        </div>
        <span class="scrollicon heartbeat"></span>
    </section>

    <section id="content" class=" mt-5 mb-5">
    <div class="container relative">
        <div class="row mt-4">
            <div class="col-md-12">
                {!! $data['main-content'] !!}
            </div>
        </div>
        <div class="row mb-5 mt-5">
            <div class="col-md-12">
                <hr>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-md-12">
                <h1 class="section-head">{!! $data['team-heading'] !!}</h1>
            </div>
        </div>
        <div class="row mt-4" id="team">
            @foreach($team as $member)
                <div class="col-md-3 member">
                    <img src="{{ asset('public/'.$member->photo) }}" width="100%">
                    <h4 class="name">{{$member->name}}</h4>
                    <h4 class="position">{{$member->title}}</h4>
                </div>
            @endforeach
        </div>
    </div>
    </section>
@endsection

@section('js')
    <script src="{{ asset('public') }}/js/inner.js"></script>

    <script>
        $('#pageslider').animate({opacity: 1}, 3000);

        //    var marginLeft = 0;
        $(window).resize(function(){
            setTimeout(function(){
                alignVideo();
            },100);
        });

        $(window).on('load',function(){
            setTimeout(function(){
                alignVideo();
            },100);
        });

        alignVideo();
        function alignVideo(){
            marginLeft = $('#video').width()/2;
            $('#video').css('margin-left','-'+(marginLeft)+'px');

//             if($('#video').width()>$(window).outerWidth()){
//                 marginLeft = $('#video').width()-$(window).outerWidth();
//                 marginLeft = marginLeft/2;
//                 $('#video').css('margin-left','-'+(marginLeft+7)+'px');
//             } else {
//                 $('#video').css('margin-left','0');
//             }
//
//             if($('.margin-top-negative').height()>$(window).outerHeight()){
//                 marginTop = $('#video').height()-$(window).outerHeight();
//                 marginTop = marginTop/2;
//
//                 $('#video').css('margin-top','-'+(marginTop+70)+'px');
//             }
//
//
//        setTimeout(function(){
//            $('#video').css('opacity',1);
//        },100);
        }

        //
        //    function alignVideo(){
        //            marginLeft = $('#video').width()/2;
        ////            $('#video').css('margin-left','-'+(marginLeft)+'px');
        //
        //             if($('#video').width()>$(window).outerWidth()){
        //                 marginLeft = $('#video').width()-$(window).outerWidth();
        //                 marginLeft = marginLeft/2;
        //                 $('#video').css('margin-left','-'+(marginLeft+7)+'px');
        //             } else {
        //                 $('#video').css('margin-left','0');
        //             }
        //
        //             if($('.margin-top-negative').height()>$(window).outerHeight()){
        //                 marginTop = $('#video').height()-$(window).outerHeight();
        //                 marginTop = marginTop/2;
        //
        //                 $('#video').css('margin-top','-'+(marginTop+70)+'px');
        //             }
        //
        //
        //        setTimeout(function(){
        //            $('#video').css('opacity',1);
        //        },100);
        //    }
        //    alignVideo();
    </script>
@endsection







