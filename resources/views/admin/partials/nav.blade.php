
@inject('contentService', 'App\Services\ContentProvider')
<?php $pages = $contentService->getPageNavs(); ?>

<nav class="sidebar sidebar-offcanvas" id="sidebar">
    <ul class="nav">
        @foreach($pages as $page)

            @if($page->id==1)
                <li class="nav-item">
                    <a class="nav-link" href="{{ URL('admin/sections/edit/1') }}">
                        <i class="menu-icon mdi mdi-home"></i>
                        <span class="menu-title">Home</span>
                    </a>
                </li>
            @else
                <li class="nav-item">
                    <a class="nav-link {{ $pageSlug==$page->slug ? 'collapsed' : ''}}" data-toggle="collapse" href="#{{$page->slug}}" aria-expanded="true" aria-controls="homepage">
                        <i class="menu-icon mdi mdi-content-copy"></i>
                        <span class="menu-title">{{ $page->name }}</span>
                        <i class="menu-arrow"></i>
                    </a>
                    <div class="collapse {{ $pageSlug==$page->slug ? 'show' : 'in'}}" id="{{$page->slug}}">
                        <ul class="nav flex-column sub-menu">
                            <li class="nav-item">
                                <a class="nav-link" href="{{ URL('admin/sections/edit/'.$page->id) }}">Page Contents</a>
                            </li>
                            @if($page->id==2)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL('admin/our-team/') }}">View Team</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL('admin/our-team/create') }}">Add Team Member</a>
                                </li>
                            @endif
                            @if($page->id==3)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL('admin/services/') }}">View Sectors</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL('admin/services/create') }}">Add Sector</a>
                                </li>
                            @endif
                            @if($page->id==4)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL('admin/clients/') }}">View Clients</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL('admin/clients/create') }}">Add Client</a>
                                </li>
                            @endif
                            @if($page->id==5)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL('admin/news/') }}">View News</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL('admin/news/create') }}">Add News</a>
                                </li>
                            @endif
                            @if($page->id==6)
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ URL('admin/contacts') }}">Inquiries</a>
                                </li>
                            @endif
                        </ul>
                    </div>
                </li>
            @endif
        @endforeach

    </ul>
</nav>
