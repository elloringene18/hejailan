@extends('master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $data = $contentService->getPageSections(3); ?>
<?php $services = $contentService->getServices(); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">
    <style>
        #pageslider {
            background-image: url({{ asset('public/'.$data['intro-image']) }});
        }

        @media only screen and (max-height: 920px) and (min-width: 1280px){
            #pageslider {
                background-size: 110% auto;
            }
        }

        .ladder-content, .ladder-box p {
            font-size: 14px;
        }

        .ladder-content a {
            font-weight: bold;
            text-decoration: underline;
        }
    </style>
@endsection


@section('content')
    <section id="pageslider">
        <div class="container relative">
            <div class="vcenter animate"  data-animation="slide-in-right-1" data-top="0">
                <h1>{!! $data['intro-heading'] !!}</h1>
            </div>
        </div>
        <span class="scrollicon heartbeat"></span>
    </section>

    <section id="content" class=" mt-5 mb-5">
        <div class="container relative">
            <div class="row mt-4">
                <div class="col-md-1"></div>
                <div class="col-md-11">
                    {!!  $data['main-content'] !!}
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-md-12">
                    @foreach($services as $service)
                    <div class="expand-box">
                        <div class="expand-head">
                            <h1 class="section-head">{{$service->name}} <a href="#" class="expand-bt"></a></h1>
                        </div>
                        <div class="expand-content">
                            <h2 class="section-sub-head mb-5">{{$service->headline}}</h2>
                            <div class="row">
                                <div class="col-md-1"></div>
                                <div class="col-md-12">
                                    {!! $service->content !!}
                                </div>
                            </div>
                            <div class="row mt-5">
                                @foreach($service->items as $item)
                                    <div class="col-md-4">
                                        <div class="ladder-box">
                                            <div class="img relative">
                                                <img src="{{ asset('public/'.$item->image) }}" width="100%;">
                                                <p class="ladder-title">{{$item->title}}</p>
                                                <div class="view-more vcenter">
                                                    <img src="{{ asset('public') }}/img/business/view-more.png">
                                                    <p>VIEW MORE</p>
                                                </div>
                                            </div>
                                            <p class="ladder-content">
                                                {!! strip_tags($item->content,'<br><a>') !!}
                                            </p>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection


@section('js')

    <script src="{{ asset('public') }}/js/inner.js"></script>
    <script>
        $('#pageslider').animate({opacity: 1}, 3000);
    </script>

@endsection
