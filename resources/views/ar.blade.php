@extends('master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $data = $contentService->getPageSections(7); ?>

@section('css')
    <style>
        #pageslider {
            font-family: 'Cairo';
        }
        #intro, #vision {
            direction: rtl;
            font-family: 'Cairo';
        }

        #pageslider .container .vcenter h1 {
            line-height: 89px;
        }
        #pageslider {
            background-image: url({{ asset('public/'.$data['intro-image']) }});
        }

        @media only screen and (max-height: 920px) and (min-width: 1280px){
            #pageslider {
                background-size: 110% auto;
            }
        }


        #pageslider .container .vcenter {
            right: 12px;
        }

        @media only screen and (max-width: 767px){
            #pageslider .container .vcenter {
                right: 0 !important;
            }
            #pageslider .container .vcenter h1 {
                line-height: 59px;
            }
        }

        section {
            direction: rtl;
            text-align: right;
        }
    </style>

    <link rel="stylesheet" href="{{ asset('public') }}/css/home.css">
@endsection


@section('content')
    <section id="pageslider">
        <div class="container relative">
            <div class="vcenter animate"  data-animation="slide-in-right-1" data-top="0">
                <h1 style="">{{ $data['intro-heading'] }} </h1>
            </div>
        </div>
        <span class="scrollicon heartbeat"></span>
        <div class="dots">
            <ul>
                <li><a href="#" class="active"></a></li>
                <li><a href="#"></a></li>
                <li><a href="#"></a></li>
            </ul>
        </div>
    </section>

    <section id="intro"  class="animate" data-animation="slide-in-left-1">
        <div class="container pt-5 mt-4 ">
            {!! $data['intro-content'] !!}
        </div>
    </section>

    <section id="vision" class="mb-5">
        <div class="container pt-5">
            <div class="row">
                <div class="col-lg-6 col-md-12">
                    <img src="{{ asset('public/'.$data['body-image']) }}" width="100%" class="mb-5 step-left">
                </div>
                <div class="col-lg-1 col-md-1">
                </div>
                <div class="col-lg-4 col-md-12 text-right animate" data-animation="slide-in-right">
                    <div class="row">
                        <div class="col-12 text-right">
                            {!! $data['image-content'] !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-1 col-md-1"></div>
            </div>
            <div class="row">
                <div class="col-md-12 mt-5">
                    {!! $data['bottom-content'] !!}
                </div>
            </div>
        </div>
    </section>
@endsection


@section('js')
@endsection
