@extends('master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $data = $contentService->getPageSections(4); ?>
<?php $clients = $contentService->getClients(); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">
    <style>
        #pageslider {
            background-image: url({{ asset('public/'.$data['intro-image']) }});
        }

        #partners img {
            margin-bottom: 10px;
        }

        #partners a {
            font-size: 12px;
        }

        #partners a:hover {
            text-decoration: underline;
        }
    </style>
@endsection


@section('content')
    <section id="pageslider">
        <div class="container relative">
            <div class="vcenter animate"  data-animation="slide-in-right-1" data-top="0">
                <h1>{!! $data['intro-heading'] !!}</h1>
            </div>
        </div>
        <span class="scrollicon heartbeat"></span>
    </section>

    <section id="content" class=" mt-5 mb-5">
        <div class="container relative">
            <div class="row mt-4">
                <div class="col-md-12">
                    {!! $data['main-content'] !!}
                </div>
            </div>

            <div class="row mt-5">
                <div class="col-md-12">
                    <hr class="mb-5" />
                    <div class="row" id="partners">
                        @foreach($clients as $client)
                        <div class="col-md-2 col-sm-4 text-center mb-5">
                            @if($client->url)
                                <a href="{{ $client->url }}" target="_blank">
                            @endif
                                <img src="{{ asset('public/'.$client->photo) }}" width="90%">
                            @if($client->url)
                                </a>
                            @endif
                            <!--                                <a href="#">News and Media ></a>-->
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection


@section('js')
    <script src="{{ asset('public') }}/js/inner.js"></script>
    <script>
        $('#pageslider').animate({opacity: 1}, 3000);
    </script>

@endsection
