@extends('master')

@inject('contentService', 'App\Services\ContentProvider')
<?php $data = $contentService->getPageSections(6); ?>

@section('css')
    <link rel="stylesheet" href="{{ asset('public') }}/css/inner.css">
    <link rel="stylesheet" href="{{ asset('public') }}/css/contact.css">
@endsection


@section('content')

    <section id="content" class=" mt-5 mb-5">
        <div class="container relative">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="section-head">{!! $data['intro-heading'] !!}</h1>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-5 mb-4">
                    <div id="mapCss">
                        <a target="_blank" href="https://www.google.com/maps/place/Al-Hejailan+Group/@24.6696607,46.7294269,19.04z/data=!4m5!3m4!1s0x3e2f05d1da8d96b1:0x2cfcfac2a060eaaf!8m2!3d24.6695749!4d46.7297056"><div id="map"></div></a>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            {!! $data['riyadh-address'] !!}
                        </div>
                        <div class="col-md-6">
                            {!! $data['dubai-address'] !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-1"></div>
                <div class="col-md-6">
                    <h5 class="">SEND US A MESSAGE</h5>
                    @if(Session::has('success'))
                        <div class="alert alert-success">{{ Session::get('success') }}</div>
                    @else
                            <form action="{{url('form-submit')}}" method="post">
                                @csrf
                                <div class="form-group">
                                    <input type="text" class="" name="name" placeholder="Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="" name="email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="" name="company" placeholder="Company">
                                </div>
                                <div class="form-group">
                                    <textarea class="" placeholder="Write us a message" name="message"></textarea>
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Submit" />
                                </div>
                            </form>
                    @endif
                </div>
            </div>
        </div>
    </section>

@endsection


@section('js')
    <script src="{{ asset('public') }}/js/inner.js"></script>
    <script>
        $('#pageslider').animate({opacity: 1}, 3000);
    </script>

    <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyChdoqnSnfKQL3byDY_Ju6MvoUD0Xds3Tk"></script>

    <script type="text/javascript">
        // When the window has finished loading create our google map below
        google.maps.event.addDomListener(window, 'load', initMap);

        function initMap() {

            var latlng = new google.maps.LatLng(24.6694161,46.7293976);

            var map = new google.maps.Map(document.getElementById('map'), {
                center: latlng,
                zoom: 13,
                styles: [{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#d3d3d3"}]},{"featureType":"transit","stylers":[{"color":"#808080"},{"visibility":"off"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"visibility":"on"},{"color":"#b3b3b3"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.local","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"weight":1.8}]},{"featureType":"road.local","elementType":"geometry.stroke","stylers":[{"color":"#d7d7d7"}]},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#ebebeb"}]},{"featureType":"administrative","elementType":"geometry","stylers":[{"color":"#a7a7a7"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"visibility":"on"},{"color":"#efefef"}]},{"featureType":"road","elementType":"labels.text.fill","stylers":[{"color":"#696969"}]},{"featureType":"administrative","elementType":"labels.text.fill","stylers":[{"visibility":"on"},{"color":"#737373"}]},{"featureType":"poi","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi","elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#d6d6d6"}]},{"featureType":"road","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{},{"featureType":"poi","elementType":"geometry.fill","stylers":[{"color":"#dadada"}]}]

            });

            // Let's also add a marker while we're at it
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(24.6694161,46.7293976),
                map: map,
                title: 'Al-Hejailan Group',
                icon: "{{ asset('public/img/marker.png') }}",
            });
        }
    </script>
@endsection
