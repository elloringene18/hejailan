$('#menu-icon').on('click',function(e){
    e.preventDefault();
    
    $(this).toggleClass('active');
    
    if($(this).hasClass('active'))
        $('#menu-nav').fadeIn();
    else
        $('#menu-nav').fadeOut();
    
    if($(this).hasClass('active'))
        $('#main-logo').addClass('active');
    else
        $('#main-logo').removeClass('active');
    
    if($(this).hasClass('active'))
        $('#head').addClass('active');
    else
        $('#head').removeClass('active');
    
	vCenter();
	hCenter();
});

function hCenter(){
	$('.hcenter').each(function(){
		mTOp = parseInt($(this).width()) / 2;
		$(this).css('margin-left','-'+mTOp+'px');
	});
}

function vCenter(){
	$('.vcenter').each(function(){
		mTOp = parseInt($(this).height()) / 2;
        
        if($(this).attr('data-top'))
		  mTOp += parseInt($(this).attr('data-top'));
            
		$(this).css('margin-top','-'+mTOp+'px');
	});
    
}

$(window).scroll(function(){
    checkScroll();
});

function checkScroll(){
    if($(document).scrollTop()>20){
        $('#head').addClass('floating');
        
        $('#main-logo-text').addClass('hide');
        $('#main-logo-text').removeClass('show');
        
        $('#main-logo-icon').addClass('rotate-center');
    }
    else {
        $('#head').removeClass('floating');
        
        $('#main-logo-text').removeClass('hide');
        $('#main-logo-text').addClass('show');
        
        $('#main-logo-icon').removeClass('rotate-center');
    }
}

$(document).ready(function(){
	vCenter();
    hCenter();
    checkScroll();
});

 $(window).on('load', function () {
	vCenter();
    hCenter();
});

 $(window).on('resize', function () {
	vCenter();
    hCenter();
});

//(function() {
//    // Add event listener
//    document.addEventListener("mousemove", parallax);
//    const elem = document.querySelector("#pageslider");
//    // Magic happens here
//    function parallax(e) {
//        let _w = window.innerWidth/2;
//        let _h = window.innerHeight/2;
//        let _mouseX = e.clientX;
//        let _mouseY = e.clientY;
//        let _depth1 = `${50 - (_mouseX - _w) * 0.003}% ${50 - (_mouseY - _h) * 0.001}%`;
//        let _depth2 = `${50 - (_mouseX - _w) * 0.04}% ${50 - (_mouseY - _h) * 0.04}%`;
//        let _depth3 = `${50 - (_mouseX - _w) * 0.09}% ${50 - (_mouseY - _h) * 0.09}%`;
//        let x = `${_depth3}, ${_depth2}, ${_depth1}`;
//        elem.style.backgroundPosition = x;
//    }
//
//})();


var x, i, j, l, ll, selElmnt, a, b, c;
/* Look for any elements with the class "custom-select": */
x = document.getElementsByClassName("custom-select");
l = x.length;
for (i = 0; i < l; i++) {
  selElmnt = x[i].getElementsByTagName("select")[0];
  ll = selElmnt.length;
  /* For each element, create a new DIV that will act as the selected item: */
  a = document.createElement("DIV");
  a.setAttribute("class", "select-selected");
  a.innerHTML = selElmnt.options[selElmnt.selectedIndex].innerHTML;
  x[i].appendChild(a);
  /* For each element, create a new DIV that will contain the option list: */
  b = document.createElement("DIV");
  b.setAttribute("class", "select-items select-hide");
  for (j = 1; j < ll; j++) {
    /* For each option in the original select element,
    create a new DIV that will act as an option item: */
    c = document.createElement("DIV");
    c.innerHTML = selElmnt.options[j].innerHTML;
    c.addEventListener("click", function(e) {
        /* When an item is clicked, update the original select box,
        and the selected item: */
        var y, i, k, s, h, sl, yl;
        s = this.parentNode.parentNode.getElementsByTagName("select")[0];
        sl = s.length;
        h = this.parentNode.previousSibling;
        for (i = 0; i < sl; i++) {
          if (s.options[i].innerHTML == this.innerHTML) {
            s.selectedIndex = i;
            h.innerHTML = this.innerHTML;
            y = this.parentNode.getElementsByClassName("same-as-selected");
            yl = y.length;
            for (k = 0; k < yl; k++) {
              y[k].removeAttribute("class");
            }
            this.setAttribute("class", "same-as-selected");
            break;
          }
        }
        h.click();
    });
    b.appendChild(c);
  }
  x[i].appendChild(b);
  a.addEventListener("click", function(e) {
    /* When the select box is clicked, close any other select boxes,
    and open/close the current select box: */
    e.stopPropagation();
    closeAllSelect(this);
    this.nextSibling.classList.toggle("select-hide");
    this.classList.toggle("select-arrow-active");
  });
}

function closeAllSelect(elmnt) {
  /* A function that will close all select boxes in the document,
  except the current select box: */
  var x, y, i, xl, yl, arrNo = [];
  x = document.getElementsByClassName("select-items");
  y = document.getElementsByClassName("select-selected");
  xl = x.length;
  yl = y.length;
  for (i = 0; i < yl; i++) {
    if (elmnt == y[i]) {
      arrNo.push(i)
    } else {
      y[i].classList.remove("select-arrow-active");
    }
  }
  for (i = 0; i < xl; i++) {
    if (arrNo.indexOf(i)) {
      x[i].classList.add("select-hide");
    }
  }
}

/* If the user clicks anywhere outside the select box,
then close all select boxes: */
document.addEventListener("click", closeAllSelect);
