<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use HasFactory;

    protected $fillable = ['title','slug','date','thumbnail','photo','content'];

    public $timestamps = ['date'];

    public function getPhotoUrlAttribute(){
        if($this->photo)
            return asset('public/'.$this->photo);

        return 'https://via.placeholder.com/1600x600';
    }

    public function getThumbnailUrlAttribute(){
        if($this->thumbnail)
            return asset('public/'.$this->thumbnail);

        return 'https://via.placeholder.com/400x240';
    }

}
