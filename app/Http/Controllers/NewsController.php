<?php

namespace App\Http\Controllers;

use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class NewsController extends Controller
{
    public function show($slug){
        $article = News::where('slug',$slug)->first();
        $metaTitle = $article->title;
        $metaDesc = Str::words(strip_tags($article->content),40);

        if($article){
            return view('article',compact('article','metaTitle','metaDesc'));
        }

        return view('404');
    }
}
