<?php

namespace App\Http\Controllers;

use App\Models\FormEntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Session;

class FormController extends Controller
{
    public function submit(Request $request){

        $input = $request->input();

        $subject = 'Hejailan Website Form Submission';

        $emailData = [
            'name' => $input['name'],
            'email' => $input['email'],
            'company' => $input['company'],
            'message' => $input['message'],
        ];

//        $emails = array("info@acota.com.au");

        $this->saveToDb($request,$emailData,'contact-us-form');

//        try {
//            Mail::send('mail.form', ['data' => $emailData], function ($message) use ($input, $subject, $emails) {
//                //
//                $message->from('info@acota.com.au', 'Admin')->to($emails)->subject($subject);
//            });
//        }
//        catch (\Exception $e) {
//            return $e->getMessage();
//        }

        Session::flash('success','Thank you for contacting us. We will get back to you shortly.');

        return redirect()->back();
    }

    public function saveToDb($request, $emailData,$form){
        $entry = FormEntry::create([
            'ip'=>$request->ip(),
            'form'=>$form
        ]);

        foreach($emailData as $key=>$item)
            $entry->items()->create([
                'slug' => $key,
                'value' => $item,
            ]);
    }
}
