<?php

namespace App\Http\Controllers\Admin;

use App\Exports\ContactExports;
use App\Http\Controllers\Controller;

use App\Models\ContactEntry;
use App\Models\FormEntry;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;

class ContactController extends Controller
{

    public function __construct(FormEntry $model)
    {
        $this->model = $model;
    }

    public function index(){
        $pageSlug = 'contacts';
        $results = FormEntry::with('items')->orderBy('id','DESC')->paginate(100);

        $data = [];

        foreach ($results as $id=>$result){
            foreach ($result->items as $valId=>$item)
                $data[$id][$item->slug] = $item->value;

            $data[$id]['id'] = $result->id;
            $data[$id]['ip'] = $result->ip;
            $data[$id]['source'] = $result->form;
            $data[$id]['date'] = $result->created_at;
        }

        return view('admin.inquiries.index',compact('data','results','pageSlug'));
    }

//    public function export(){
//        return Excel::download(new ContactExports(), 'AMSI-Inquiries.xlsx');
//    }

    public function delete($id){
        $page = $this->model->find($id);

        if($page)
            $page->delete();

        return redirect()->back();
    }

    public function view($id){
        $pageSlug = 'contacts';
        $results = $this->model->with('items')->find($id);

        $data = [];

        foreach ($results->items as $valId=>$item)
            $data[$item->slug] = $item->value;

        $data['id'] = $results->id;
        $data['ip'] = $results->ip;
        $data['source'] = $results->form;
        $data['date'] = $results->created_at;

        return view('admin.inquiries.view',compact('data','pageSlug'));
    }


}
