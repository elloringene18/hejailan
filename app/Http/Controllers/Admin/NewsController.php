<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\News;
use App\Models\Page;
use App\Models\PageSection;
use App\Services\CanCreateSlug;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;

class NewsController extends Controller
{
    use CanCreateSlug;

    public function __construct(News $model)
    {
        $this->model = $model;
    }

    public function index(){
        $posts = News::get();
        $pageSlug = 'news-updates';

        return view('admin.news.index', compact('posts','pageSlug'));
    }

    public function create(){
        $pageSlug = 'news-updates';
        return view('admin.news.create',compact('pageSlug'));
    }

    public function store(Request $request){
        $input['title'] = $request->input('title');
        $input['content'] = $request->input('content');
        $input['slug'] = $this->generateSlug($request->input('title'));

        $file = $request->file('photo');

        if($file){
            $destinationPath = 'public/uploads/news';
            $thumbnailName =  Str::random(32).'.'.$file->getClientOriginalExtension();
            Image::make($file->getRealPath())->fit(545, 435)->save($destinationPath.'/'.$thumbnailName);
            $input['photo'] = 'uploads/news/'. $thumbnailName;

            $thumbnailName =  Str::random(32).'.'.$file->getClientOriginalExtension();
            Image::make($file->getRealPath())->fit(1366, 768)->save($destinationPath.'/'.$thumbnailName);
            $input['thumbnail'] = 'uploads/news/'. $thumbnailName;
        }

        News::create($input);
        return redirect(url('admin/news'));
    }

    public function edit($id){
        $pageSlug = 'news-updates';
        $data = News::find($id);

        return view('admin.news.edit', compact('data','pageSlug'));
    }

    public function update(Request $request){
        $target = News::find($request->input('id'));

        $input['title'] = $request->input('title');
        $input['content'] = $request->input('content');

        if($target->title != $input['title'])
            $input['slug'] = $this->generateSlug($request->input('title'));

        $file = $request->file('photo');

        if($file){
            $destinationPath = 'public/uploads/news';
            $thumbnailName =  Str::random(32).'.'.$file->getClientOriginalExtension();
            Image::make($file->getRealPath())->fit(545, 435)->save($destinationPath.'/'.$thumbnailName);
            $input['photo'] = 'uploads/news/'. $thumbnailName;

            $thumbnailName =  Str::random(32).'.'.$file->getClientOriginalExtension();
            Image::make($file->getRealPath())->fit(1366, 768)->save($destinationPath.'/'.$thumbnailName);
            $input['thumbnail'] = 'uploads/news/'. $thumbnailName;
        }

        $target->update($input);

        return redirect()->back();
    }

    public function delete($id){
        $data = News::find($id);

        if($data){
            $data->delete();
        }

        return redirect(url('admin/news'));
    }
}
